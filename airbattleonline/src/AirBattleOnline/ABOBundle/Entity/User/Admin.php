<?php
/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of airbattleonline.
 *
 * airbattleonline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * airbattleonline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with airbattleonline.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AirBattleOnline\ABOBundle\Entity\User;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Admin.php
 *
 * @version May 28, 2014
 * @author Adrián López González
 *        
 * @ORM\Entity
 * @UniqueEntity("username")
 */
class Admin implements UserInterface {
	
	/* Fields */
	/**
	 * @ORM\Id
	 * @ORM\Column(type = "integer")
	 * @ORM\GeneratedValue
	 */
	private $id;
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $username;
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $password;
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $salt;
	/**
	 * @ORM\Column(type="string", length=255)
	 *
	 * @Assert\Email(checkMX=true)
	 */
	private $email;
	/**
	 * @ORM\Column(type = "datetime")
	 */
	private $registered;
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $activation_key;
	/**
	 * @ORM\Column(type = "integer")
	 */
	private $status;
	/**
	 * @ORM\Column(type = "integer")
	 */
	private $score;
	
	// METHODS
	/**
	 * Erease credentials.
	 */
	public function eraseCredentials() {
		// nop;
	}
	
	/**
	 * Get roles of this user
	 */
	public function getRoles() {
		return array ("ROLE_ADMIN");
	}
	
	// GETTERS & SETTERS
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Set username
	 *
	 * @param string $username        	
	 * @return User
	 */
	public function setUsername($username) {
		$this->username = $username;
		
		return $this;
	}
	
	/**
	 * Get username
	 *
	 * @return string
	 */
	public function getUsername() {
		return $this->username;
	}
	
	/**
	 * Set password
	 *
	 * @param string $password        	
	 * @return User
	 */
	public function setPassword($password) {
		$this->password = $password;
		
		return $this;
	}
	
	/**
	 * Get password
	 *
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}
	
	/**
	 * Set salt
	 *
	 * @param string $salt        	
	 * @return User
	 */
	public function setSalt($salt) {
		$this->salt = $salt;
		
		return $this;
	}
	
	/**
	 * Get salt
	 *
	 * @return string
	 */
	public function getSalt() {
		return $this->salt;
	}
	
	/**
	 * Set email
	 *
	 * @param string $email        	
	 * @return User
	 */
	public function setEmail($email) {
		$this->email = $email;
		
		return $this;
	}
	
	/**
	 * Get email
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	
	/**
	 * Set registered
	 *
	 * @param \DateTime $registered        	
	 * @return User
	 */
	public function setRegistered($registered) {
		$this->registered = $registered;
		
		return $this;
	}
	
	/**
	 * Get registered
	 *
	 * @return \DateTime
	 */
	public function getRegistered() {
		return $this->registered;
	}
	
	/**
	 * Set activation_key
	 *
	 * @param string $activationKey        	
	 * @return User
	 */
	public function setActivationKey($activationKey) {
		$this->activation_key = $activationKey;
		
		return $this;
	}
	
	/**
	 * Get activation_key
	 *
	 * @return string
	 */
	public function getActivationKey() {
		return $this->activation_key;
	}
	
	/**
	 * Set status
	 *
	 * @param integer $status        	
	 * @return User
	 */
	public function setStatus($status) {
		$this->status = $status;
		
		return $this;
	}
	
	/**
	 * Get status
	 *
	 * @return integer
	 */
	public function getStatus() {
		return $this->status;
	}
	
	/**
	 * Set score
	 *
	 * @param integer $score        	
	 * @return User
	 */
	public function setScore($score) {
		$this->score = $score;
		
		return $this;
	}
	
	/**
	 * Get score
	 *
	 * @return integer
	 */
	public function getScore() {
		return $this->score;
	}
	
	// TO STRING
	public function __toString() {
		return $this->getName ();
	}
}