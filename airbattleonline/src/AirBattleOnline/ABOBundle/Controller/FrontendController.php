<?php
/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of airbattleonline.
 *
 * airbattleonline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * airbattleonline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with airbattleonline.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AirBattleOnline\ABOBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// FORM
use DEIDEASMarketing\ECBundle\Form\RegistryPlayerFormType;
// ENTITIES
use DEIDEASMarketing\ECBundle\Entity\User\Player;

/**
 * FrontendController.php
 *
 * @version May 29, 2014
 * @author Adrián López González 
 */
class FrontendController extends Controller {

	/**
	 * Render home.
	 */
	public function homeAction() {
		return $this->render('ABOBundle:Frontend:home.html.twig');
	}
	
	/**
	 * Function to registry a propietary.
	 */
	public function registryPlayerAction() {
		
	}
}