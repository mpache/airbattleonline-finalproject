<?php
/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of airbattleonline.
 *
 * airbattleonline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * airbattleonline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with airbattleonline.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AirBattleOnline\ABOBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
// FORM
use AirBattleOnline\ABOBundle\Form\RegistryPlayerFormType;
// ENTITIES
use AirBattleOnline\ABOBundle\Entity\User\Player;

/**
 * GameController.php
 *
 * @version May 29, 2014
 * @author Adrián López González 
 */
class GameController extends Controller {

	/**
	 * Render home of game
	 */
	public function homeAction() {
		// Get user.
		return $this->render('ABOBundle:Game:home.html.twig', array(
			'username' => $this->getUser()
		));
	}
	
	/**
	 * Function to login game.
	 */
	public function loginAction() {
		if ($this->get('security.context')->isGranted('ROLE_PLAYER')
				|| $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			return $this->redirect ( $this->generateUrl ( 'abo_game' ) );
		} else {
			$request = $this->getRequest();
			$session = $request->getSession();
				
			$error = $request->attributes->get(
					SecurityContext::AUTHENTICATION_ERROR,
					$session->get(SecurityContext::AUTHENTICATION_ERROR)
			);
				
			return $this->render('ABOBundle:Game:login.html.twig', array(
					'last_username' => $session->get(SecurityContext::LAST_USERNAME),
					'error' => $error
			));
		}
	}
}