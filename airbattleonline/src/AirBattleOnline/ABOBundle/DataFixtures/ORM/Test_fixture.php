<?php
/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of airbattleonline.
 *
 * airbattleonline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * airbattleonline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with airbattleonline.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AirBattleOnline\ABOBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
// Entities
use AirBattleOnline\ABOBundle\Entity\User\Admin;
use AirBattleOnline\ABOBundle\Entity\User\Player;

/**
 * Test_fixture.php
 *
 * @version May 29, 2014
 * @author Adrián López González 
 */
class Test_fixture implements ContainerAwareInterface, FixtureInterface {
	
	/* Fields */
	
	/**
	 * Set container.
	 * 
	 * @param ContainerInterface $container
	 */
	public function setContainer(ContainerInterface $container = null) {
		$this->container = $container;
	}
	
	/**
	 * load test fixtures.
	 *
	 * @param ObjectManager $manager        	
	 */
	public function load(ObjectManager $manager) {
		$this->createTestPlayer($manager);
		$this->createTestAdmin($manager);
	}
	
	// Private methods
	/** Function to create test data of propietary entity */
	private function createTestPlayer(ObjectManager $manager) {
		// Create Player
		$player = new Player();
		// Put password and create salt.
		$password = '123';
		$salt = md5(time());
		// encrypt password.
		$encoder = $this->container->get('security.encoder_factory')->getEncoder($player);
		$encrypted_password = $encoder->encodePassword($password, $salt);
		// Put values of propietari
		$player->setUsername('test');
		$player->setPassword($encrypted_password);
		$player->setSalt($salt);
		$player->setEmail("adrian.lg.456@gmail.com");
		$player->setRegistered(new \DateTime());
		$player->setActivationKey("none");
		$player->setStatus("0");
		$player->setScore("0");
		// Save propietary in database
		$manager->persist($player);
		$manager->flush();
	}
	
	/** Function to create test data of locations entities */
	private function createTestAdmin(ObjectManager $manager) {
		// Create Player 1
		$admin = new Admin();
		// Put password and create salt.
		$password = '123';
		$salt = md5(time());
		// encrypt password.
		$encoder = $this->container->get('security.encoder_factory')->getEncoder($admin);
		$encrypted_password = $encoder->encodePassword($password, $salt);
		// Put values of propietari
		$admin->setUsername('Happy');
		$admin->setPassword($encrypted_password);
		$admin->setSalt($salt);
		$admin->setEmail("adrian.lg.456@gmail.com");
		$admin->setRegistered(new \DateTime());
		$admin->setActivationKey("none");
		$admin->setStatus("0");
		$admin->setScore("0");
		// Save propietary in database
		$manager->persist($admin);
		$manager->flush();
	}
}