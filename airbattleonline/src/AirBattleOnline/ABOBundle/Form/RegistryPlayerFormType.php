<?php
/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of airbattleonline.
 *
 * airbattleonline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * airbattleonline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with airbattleonline.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AirBattleOnline\ABOBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * RegistryPlayerFromType.php
 *
 * @version May 28, 2014
 * @author Adrián López González
 */
class RegistryPlayerFromType extends AbstractType {
	
	/**
	 * Build form.
	 * 
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add ( 'username' )
			->add('password', 'repeated', array(
					'type' => 'password',
					'invalid_message' => 'Las dos contraseñas deben coincidir',
					'first_options'  => array('label' => false),
					'second_options' => array('label' => false),
			))
			->add('email', 'email');
	}
	
	/**
	 * Set default option,s
	 * 
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults ( array (
				'data_class' => 'AirBattleOnline\ABOBundle\Entity\User\Player' 
		) );
	}
	
	/**
	 * Get name of form.
	 * 
	 * @return string
	 */
	public function getName() {
		return 'airbattleonline_abobundle_registryplayerformtype';
	}
}
