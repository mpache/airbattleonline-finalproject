/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * main.js
 * 
 * @version May 26, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */

var fs = require('fs'), io = require('socket.io').listen(8080);

var numRooms = 0;
var config = null;

function main() {
    var Log = require('log');
    worlds = [], lastTotalPlayers = 0;

    switch (config.debug_level) {
    case "error":
	log = new Log(Log.ERROR);
	break;
    case "debug":
	log = new Log(Log.DEBUG);
	break;
    case "info":
	log = new Log(Log.INFO);
	break;
    }
    ;

    log.info("Starting Air Batlle game server...");
}

function getWorldDistribution(worlds) {
    var distribution = [];

    _.each(worlds, function(world) {
	distribution.push(world.playerCount);
    });
    return distribution;
}

function getConfigFile(path, callback) {
    fs.readFile(path, 'utf8', function(err, json_string) {
	if (err) {
	    console.error("Could not open config file:", err.path);
	    callback(null);
	} else {
	    callback(JSON.parse(json_string));
	}
    });
}

var defaultConfigPath = './server/config.json', customConfigPath = './server/config_local.json';

process.argv.forEach(function(val, index, array) {
    if (index === 2) {
	customConfigPath = val;
    }
});

getConfigFile(
	defaultConfigPath,
	function(defaultConfig) {
	    getConfigFile(
		    customConfigPath,
		    function(localConfig) {
			if (localConfig) {
			    config = localConfig;
			    main();
			} else if (defaultConfig) {
			    config = defaultConfig;
			    main();
			} else {
			    console
				    .error("Server cannot start without any configuration file.");
			    process.exit(1);
			}
		    });
	});

function numberOfUsers(room) {
    var numPlayers = 0;
    var clients = io.sockets.adapter.rooms[room];
    for ( var clientId in clients) {
	numPlayers++;
    }
    return numPlayers;
}

io.sockets.on('connection', function(socket) {
    var room = 'room' + numRooms;
    var numPlayers = numberOfUsers(room);

    while (config.nb_players_per_world <= numPlayers
	    && numRooms < config.nb_worlds) {
	numRooms++;
	room = 'room' + numRooms;
	numPlayers = numberOfUsers(room);
    }

    if (numRooms >= config.nb_worlds) {
	socket.emit('alert', "The game is full of players try again later");
    } else {
	socket.join(room);
	numPlayers++;
	io.sockets.emit('connected', {
	    players : numPlayers,
	    total : config.nb_players_per_world
	});
    }

    socket.on('message', function(data) {
	console.log(data);
    });

    socket.on('initEnemies', function(data) {
	var playerRoom = socket.rooms[1];
	socket.myappsuperuserid = data.id;
	socket.broadcast.to(playerRoom).emit('addEnemies', data);
    });

    socket.on('updateEnemies', function(data) {
	var playerRoom = socket.rooms[1];
	socket.broadcast.to(playerRoom).emit('updateEnemies', data);
    });

    socket.on('initProjectiles', function(data) {
	var playerRoom = socket.rooms[1];
	socket.myappsuperuserid = data.id;
	socket.broadcast.to(playerRoom).emit('addProjectiles', data);
    });

    socket.on('updateProjectiles', function(data) {
	var playerRoom = socket.rooms[1];
	socket.broadcast.to(playerRoom).emit('updateProjectiles', data);
    });

    socket.on('disconnect', function() {
	var numPlayers = numberOfUsers(socket.rooms[1]);
	var playerRoom = socket.rooms[1];
	io.sockets.emit('connected', {
	    players : numPlayers,
	    total : config.nb_players_per_world
	});
	socket.broadcast.to(playerRoom).emit('removeEnemies',
		socket.myappsuperuserid);
    });
});
