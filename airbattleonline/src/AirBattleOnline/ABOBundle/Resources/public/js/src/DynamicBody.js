/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * DynamicBody.js
 * 
 * Abstract class
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var DynamicBody = function(x, y, z, w, h, d, is3D) {
    // Prevent this class is instantiated as it is abstract.
    if (this.constructor === DynamicBody) {
	throw new Error("Abstract class may not be instanciated.");
	return;
    }
    Body.call(this, x, y, z, w, h, d, is3D);

    /* Fields */
    this.velocity = is3D ? new THREE.Vector3(0, 0, 0) : new THREE.Vector2(0, 0);
};

//Extends of Body.
DynamicBody.prototype = Object.create(Body.prototype);
DynamicBody.prototype.constructor = DynamicBody;