/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * InputGameLoop.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var InputGameLoop = function(c) {

    /* Fields */
    var controller = c;

    /**
     * Check input.
     */
    this.checkInput = function() {
	keyPressed();
    };

    var keyPressed = function() {
	if (KeyboardTracker.isKeyDown(KeyboardTracker.RIGHT)) {
	    controller.processAction(KeyboardTracker.RIGHT);
	}
	if (KeyboardTracker.isKeyDown(KeyboardTracker.LEFT)) {
	    controller.processAction(KeyboardTracker.LEFT);
	}
	if (KeyboardTracker.isKeyDown(KeyboardTracker.UP)) {
	    controller.processAction(KeyboardTracker.UP);
	}
	if (KeyboardTracker.isKeyDown(KeyboardTracker.DOWN)) {
	    controller.processAction(KeyboardTracker.DOWN);
	}
	if (KeyboardTracker.isKeyDown(KeyboardTracker.SPACE)) {
	    controller.processAction(KeyboardTracker.SPACE);
	}
	if (KeyboardTracker.isKeyDown(KeyboardTracker.SPACE)) {
	    controller.processAction(KeyboardTracker.SPACE);
	}
	if (KeyboardTracker.isKeyDown(KeyboardTracker.ARROW_UP)) {
	    controller.processAction(KeyboardTracker.ARROW_UP);
	}
	if (KeyboardTracker.isKeyDown(KeyboardTracker.ARROW_DOWN)) {
	    controller.processAction(KeyboardTracker.ARROW_DOWN);
	}
    };
};