/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline2d.
 *
 * AirBattleOnline2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline2d.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @namespace */
var Resizer = Resizer || {};

/**
 * Update renderer and camera when the window is resized
 * 
 * @param {Object}
 *                renderer the renderer to update.
 * @param {Object}
 *                Camera the camera to update.
 * @param {Object}
 *                Width the width to update. 
 * @param {Object}
 *                Height the height to update.            
 */
Resizer.WindowResize = function(renderer, camera, width, height) {
    // notify the renderer of the size change
    renderer.setSize(width, height);
    // update the camera
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
};