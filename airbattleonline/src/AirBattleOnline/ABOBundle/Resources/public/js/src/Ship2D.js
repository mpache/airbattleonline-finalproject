/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 * 		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Ship2D.js
 * 
 * Abstract class.
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var Ship2D = function(x, y, w, h) {
    DynamicBody.call(this, x, y, 0, w, h, 0, false);

    /* Constants */
    var SPEED = 0.6;
    var DECELERATION = 0.9;
    var IMMUNITY_TIME = 1; // time in seconds
    var EXPLODE_TIME = 0.5; // time in seconds
    var RESPAWN_TIME = 1; // time in seconds
    var DECREASE_OVERHEATING = 1; // Units.

    /* Fields */
    // Count downs.
    var immunityCD = IMMUNITY_TIME;
    var explodeCD = 0;
    var respawnCD = 0;

    // State and mesh
    var state = null;
    var mesh = null;
    // life
    var maxLife = 100;
    var life = maxLife;
    // Shield
    var maxShield = 100;
    var shield = 0;
    // Overheating.
    var maxOverheating = 100;
    var overheating = 0;
    // Weapons
    var actualWeapon = null;
    var weapons = [];
    // score
    var score = 0;

    // Angle to rotate ship.
    var angle = 0;
    var targetAngle = 0;

    /**
     * Speed up.
     */
    this.speedUp = function() {
	this.velocity.multiplyScalar(0.96);
	this.velocity.x += Math.cos(angle) * SPEED;
	this.velocity.y += Math.sin(angle) * SPEED;
    };

    /**
     * Speed down.
     */
    this.speedDown = function() {
	this.velocity.multiplyScalar(0.96);
	this.velocity.multiplyScalar(DECELERATION);
    };

    /**
     * Rotate to left.
     */
    this.rotateLeft = function() {
	this.velocity.multiplyScalar(0.96);
	targetAngle -= THREE.Math.degToRad(5);
	calculateRotation();
    };

    /**
     * Rotate to right.
     */
    this.rotateRight = function() {
	this.velocity.multiplyScalar(0.96);
	targetAngle += THREE.Math.degToRad(5);
	calculateRotation();
    };

    /**
     * Get state of ship.
     * 
     * @returns the state of ship.
     */
    this.getState = function() {
	return state;
    };

    /**
     * Get mesh that represents this ship.
     * 
     * @returns the mesh of this sip.
     */
    this.getMesh = function() {
	return mesh;
    };
    
    /**
     * Set the mesh of this ship
     * 
     * @param newMesh is the new mesh of ship.
     */
    this.setMesh = function(newMesh) {
	mesh = newMesh;
    };
    
    /**
     * Get maximum life of ship
     * 
     * @returns the maximum life of ship.
     */
    this.getMaxLife = function() {
	return maxLife;
    };

    /**
     * Get life of ship.
     * 
     * @returns the life remaining of ship.
     */
    this.getLife = function() {
	return life;
    };

    /**
     * Add life to ship.
     * 
     * @param lifeToAdd
     *                is the life to add to this ship.
     */
    this.addLife = function(lifeToAdd) {
	life = life + lifeToAdd >= maxLife ? maxLife : life + lifeToAdd;
    };

    /**
     * Remove life to ship
     * 
     * @param lifeToRemove
     *                is the life to remove to this ship.
     */
    this.removeLife = function(lifeToRemove) {
	if (shield > 0) {
	    var tmp = shield;
	    shield = shield - lifeToRemove >= 0 ? shield - lifeToRemove : 0;
	    lifeToRemove = tmp - lifeToRemove >= 0 ? 0 : lifeToRemove - shield;
	}
	life = life - lifeToRemove > 0 ? life - lifeToRemove : 0;
    };

    /**
     * Get max shield.
     * 
     * @returns the maximum max shield.
     */
    this.getMaxShield = function() {
	return maxShield;
    };

    /**
     * Get shield of ship.
     */
    this.getShield = function() {
	return shield;
    };

    /**
     * Add shield to this ship-
     * 
     * @param shieldToAdd
     *                is the shield to add to this ship.
     */
    this.addShield = function(shieldToAdd) {
	shield = shield + shieldToAdd >= maxShield ? maxShield : shield
		+ shieldToAdd;
    };

    /**
     * Get maximum overheating.
     * 
     * @returns the maximum overheating.
     */
    this.getMaxOverheating = function() {
	return maxOverheating;
    };

    /**
     * Get overheating of ship.
     * 
     * @returns the actual overheating of this ship.
     */
    this.getOverheating = function() {
	return overheating;
    };

    /**
     * Increase overheating.
     * 
     * @param units
     *                is the number of units to add to overheating.
     */
    this.increaseOverheating = function(units) {
	overheating = overheating + units >= maxOverheating ? maxOverheating
		: overheating + units;
    };

    /**
     * Decrease overheating.
     */
    this.decreaseOverheating = function() {
	overheating = overheating - DECREASE_OVERHEATING <= 0 ? 0 : overheating
		- DECREASE_OVERHEATING;
    };

    /**
     * Get invincible count down.
     * 
     * @returns the actual invicnible count down.
     */
    this.getImmunityCD = function() {
	return immunityCD;
    };

    /**
     * Add immunity to this ship.
     */
    this.addImmunityCD = function() {
	immunityCD = IMMUNITY_TIME;
    };

    /**
     * Decrease immunity.
     * 
     * @param delta
     *                is the time elapsed between frames.
     */
    this.decreaseImmunityCD = function(delta) {
	immunityCD = immunityCD - delta > 0 ? immunityCD - delta : 0;
    };

    /**
     * Get explode count down.
     * 
     * @returns the explode count down.
     */
    this.getExplodeCD = function() {
	return explodeCD;
    };

    /**
     * Add explode time.
     */
    this.addExplodeCD = function() {
	explodeCD = EXPLODE_TIME;
    };

    /**
     * Decrease explode count down.
     * 
     * @param delta
     *                is the time elapsed between frames.
     */
    this.decreaseExplodeCD = function(delta) {
	explodeCD = explodeCD - delta > 0 ? explodeCD - delta : 0;
    };

    /**
     * Get respawn count down.
     * 
     * @returns the respawn count down.
     */
    this.getRespawnCD = function() {
	return respawnCD;
    };

    /**
     * Add respawn time.
     */
    this.addRespawnCD = function() {
	respawnCD = RESPAWN_TIME;
    };

    /**
     * Decrease respawn count down.
     * 
     * @param delta
     *                is the time elapsed between frames.
     */
    this.decreaseRespawnCD = function(delta) {
	respawnCD = respawnCD - delta > 0 ? respawnCD - delta : 0;
    };

    /**
     * Get angle of this ship.
     * 
     * @returns the angle of this ship.
     */
    this.getAngle = function() {
	return angle;
    };

    // PRIVATE METHOD
    var calculateRotation = function() {
	if (targetAngle > angle + Math.PI)
	    targetAngle -= Math.PI * 2;
	if (targetAngle < angle - Math.PI)
	    targetAngle += Math.PI * 2;
	angle += (targetAngle - angle) * 0.4;
    };
};

// Extends of DynamicBody.
Ship2D.prototype = Object.create(DynamicBody.prototype);
Ship2D.prototype.constructor = Ship2D;