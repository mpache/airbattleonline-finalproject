/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ShipEntity.js
 * 
 * @version June 3, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var ShipEntity = function(shipType, px, py, pz, w, h, d, l, s, sp, dw) {

    /* Fields */
    this.type = shipType;
    this.posx = px;
    this.posy = py;
    this.posz = pz;
    this.width = w;
    this.height = h;
    this.depth = d;
    this.life = l;
    this.shield = s;
    this.speed = sp;
    this.defaultWeapon = dw;

    /**
     * To string.
     */
    this.toString = function() {
	return "ShipEntity [type=" + this.type + ", posx=" + this.posx
		+ ", posy=" + this.posy + ", posz=" + this.posz + ", width="
		+ this.width + ", height=" + this.height + ", depth="
		+ this.depth + ", life=" + this.life + ", shield="
		+ this.shield + ", speed=" + this.speed + ", weapon="
		+ this.defaultWeapon + "]";
    };
};