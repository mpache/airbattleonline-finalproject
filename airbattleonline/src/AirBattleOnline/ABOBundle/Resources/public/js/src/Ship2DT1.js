/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 * 		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Ship2DT1.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var Ship2DT1 = function(x, y, w, h, l, s, sp) {
    Ship.call(this, x, y, 0, w, h, 0, l, s, false);

    /* Fields */
    var SPEED = sp;
    var DECELERATION = 0.9;
    // Angle to rotate.
    var targetAngle = 0;

    /**
     * Speed up.
     */
    this.speedUp = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // Speed up.
	    this.setState(ShipState.SPEED_UP);
	    this.velocity.multiplyScalar(0.96);
	    this.velocity.x += Math.cos(this.getAngleX()) * SPEED;
	    this.velocity.y += Math.sin(this.getAngleX()) * SPEED;
	}
    };

    /**
     * Speed down.
     */
    this.speedDown = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // Speed down.
	    this.setState(ShipState.SPEED_DOWN);
	    this.velocity.multiplyScalar(0.96);
	    this.velocity.multiplyScalar(DECELERATION);
	}
    };

    /**
     * Rotate to up.
     */
    this.rotateUp = function() {
	// Nop
    };

    /**
     * Rotate to down.
     */
    this.rotateDown = function() {
	// Nop
    };

    /**
     * Rotate to left.
     */
    this.rotateLeft = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // rotate left
	    this.setState(ShipState.ROTATE);
	    this.velocity.multiplyScalar(0.96);
	    targetAngle -= THREE.Math.degToRad(5);
	    var angle = this.calculateRotation(targetAngle, this.getAngleX());
	    this.setAngleX(angle);
	}
    };

    /**
     * Rotate to right.
     */
    this.rotateRight = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // rotate right
	    this.setState(ShipState.ROTATE);
	    this.velocity.multiplyScalar(0.96);
	    targetAngle += THREE.Math.degToRad(5);
	    var angle = this.calculateRotation(targetAngle, this.getAngleX());
	    this.setAngleX(angle);
	}
    };

    /**
     * Shoot.
     */
    this.shoot = function(delta) {
	if (!this.isOverheating() && this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    if (this.getTime() >= 0.5) {
		this.resetTime();
		var w = this.getActualWeapon();
		var p = new Projectile(this, this.position.x, this.position.y,
			this.position.z, this.getAngleX(), this.getAngleY(),
			this.getAngleZ(), w.getType(), w.getWidth(), w
				.getHeight(), w.getDepth(), w.getSpeed(), w
				.getDamage(), w.getDuration(), false);
		this.getWorld().addProjectile(p);
		this.getWorld().sendProjectile(p);
		// Add overheating.
		this.increaseOverheating(w.getDamage());
	    }
	}
    };

    // GETTERS & SETTERS
    /**
     * Get speed of ship.
     * 
     * @returns the speed of ship.
     */
    this.getSpeed = function() {
	return SPEED;
    };

    // TO STRING
    /** @Override */
    this.toString = function() {
	return ShipType.SHIP2D_T1;
    };
};

// Extends of Ship.
Ship2DT1.prototype = Object.create(Ship.prototype);
Ship2DT1.prototype.constructor = Ship2DT1;