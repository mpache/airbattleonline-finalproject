/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * World2D.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var World2D = function() {

    /* Fields */
    this.WIDTH = 2000;
    this.HEIGHT = 2000;

    // Star system
    var starSystem = null;

    // Associations
    var player = new Ship2D(THREE.Math.randInt(0, this.WIDTH), THREE.Math
	    .randInt(0, this.HEIGHT), 40, 40); // Test
    var enemies = [];
    var projectiles = [];
    var items = [];
    var blocks = [];

    /**
     * Update player.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updatePlayer = function(delta) {
	player.position.add(player.velocity);

	this.wolrdBounds(player);
	// this.position.add(this.velocity);
	// // Check if ship is invencible.
	// if (invincibleCountdown > 0)
	// invincibleCountdown--;
	// // Limits screen
	// while (this.position.x < 0)
	// this.position.x += world.WIDTH;
	// while (this.position.y < 0)
	// this.position.y += world.HEIGHT;
	// this.position.x = this.position.x % world.WIDTH;
	// this.position.y = this.position.y % world.HEIGHT;
	// // Rotate ship
	// if (targetAngle > angle + Math.PI)
	// targetAngle -= Math.PI * 2;
	// if (targetAngle < angle - Math.PI)
	// targetAngle += Math.PI * 2;
	// angle += (targetAngle - angle) * 0.4;
	// // Check if ship is dead.
	// if (life < 0) {
	// life = 0;
	// explodeCountDown = 10;
	// }
    };

    /**
     * Update enemies.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updateEnemies = function(delta) {
	// TODO
    };

    /**
     * Update projectiles.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updateProjectiles = function(delta) {

    };

    /**
     * Update items.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updateItems = function(delta) {

    };

    /**
     * Update blocks.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updateBlocks = function(delta) {

    };

    /**
     * Limits of world.
     * 
     * @param body
     *                is a body.
     */
    this.wolrdBounds = function(body) {
	// Limits screen
	while (body.position.x < 0)
	    body.position.x += this.WIDTH;
	while (body.position.y < 0)
	    body.position.y += this.HEIGHT;
	body.position.x = body.position.x % this.WIDTH;
	body.position.y = body.position.y % this.HEIGHT;
    };

    // -----------------------------------------
    // Management of Associations
    // -----------------------------------------
    /**
     * Get star system.
     * 
     * @return the star system.
     */
    this.getStarSystem = function() {
	return starSystem;
    };

    /**
     * Set the star system
     * 
     * @param newStarSystem
     *                is the new star system.
     */
    this.setStarSystem = function(newStarSystem) {
	starSystem = newStarSystem;
    };

    /**
     * Get player.
     * 
     * @return the player
     */
    this.getPlayer = function() {
	return player;
    };

    /**
     * Set player
     * 
     * @param newPlayer
     *                is the new player to add.
     * @return true if player has changed, false otherwise.
     */
    this.setPlayer = function(newPlayer) {
	if (player == newPlayer)
	    return false;
	var oldPlayer = player;
	player = newPlayer;
	if (oldPlayer != null) {
	    oldPlayer.setWorld(null);
	}
	if (player != null) {
	    player.setWorld(this);
	}
	return true;
    };

    /**
     * Get enemies.
     * 
     * @return the enemies.
     */
    this.getEnemies = function() {
	return enemies;
    };

    /**
     * Add enemy.
     * 
     * @param enemy
     *                is the enemy to add.
     * @return true if enemy has be added, false otherwise.
     */
    this.addEnemy = function(enemy) {
	if (enemy != null) {
	    enemies.push(enemy);
	    return true;
	}
	return false;
    };

    /**
     * Remove a specific enemy.
     * 
     * @param enemy
     *                is the enemy to remove.
     * @return true if enemy has be removed, false otherwise.
     */
    this.removeEnemy = function(enemy) {
	if (enemy != null) {
	    var index = enemies.indexOf(enemy);
	    if (index != -1) {
		enemies = enemies.splice(index, 1);
		return true;
	    }
	    return false;
	}
	return false;
    };

    /**
     * Calculate the number of enemies that in world.
     * 
     * @return the number of enemies that in world.
     */
    this.enemiesSize = function() {
	return enemies.length();
    };

    /**
     * Get items.
     * 
     * @return the items.
     */
    this.getItems = function() {
	return items;
    };

    /**
     * Add item.
     * 
     * @param item
     *                is the item to add.
     * @return true if item has be added, false otherwise.
     */
    this.addItem = function(item) {
	if (item != null) {
	    items.push(item);
	    return true;
	}
	return false;
    };

    /**
     * Remove a specific item.
     * 
     * @param item
     *                is the item to remove.
     * @return true if item has be removed, false otherwise.
     */
    this.removeItem = function(item) {
	if (item != null) {
	    var index = items.indexOf(item);
	    if (index != -1) {
		items = items.splice(index, 1);
		return true;
	    }
	    return false;
	}
	return false;
    };

    /**
     * Calculate the number of items that in world.
     * 
     * @return the number of items that in world.
     */
    this.itemsSize = function() {
	return items.length();
    };

    /**
     * Get projectiles.
     * 
     * @return the projectiles.
     */
    this.getProjectiles = function() {
	return projectiles;
    };

    /**
     * Add projectile.
     * 
     * @param projectile
     *                is the projectile to add.
     * @return true if projectile has be added, false otherwise.
     */
    this.addProjectile = function(projectile) {
	if (projectile != null) {
	    projectiles.push(projectile);
	    return true;
	}
	return false;
    };

    /**
     * Remove a specific projectile.
     * 
     * @param projectile
     *                is the projectile to remove.
     * @return true if projectile has be removed, false otherwise.
     */
    this.removeProjectile = function(projectile) {
	if (projectile != null) {
	    var index = projectiles.indexOf(projectile);
	    if (index != -1) {
		projectiles = projectiles.splice(index, 1);
		return true;
	    }
	    return false;
	}
	return false;
    };

    /**
     * Calculate the number of enemies that in world.
     * 
     * @return the number of enemies that in world.
     */
    this.projectilesSize = function() {
	return projectiles.length();
    };

    /**
     * Get blocks.
     * 
     * @return the enemies.
     */
    this.getBlocks = function() {
	return blocks;
    };

    /**
     * Add block.
     * 
     * @param block
     *                is the block to add.
     * @return true if block has be added, false otherwise.
     */
    this.addBlock = function(block) {
	if (block != null) {
	    blocks.push(block);
	    return true;
	}
	return false;
    };

    /**
     * Remove a specific block.
     * 
     * @param block
     *                is the block to remove.
     * @return true if block has be removed, false otherwise.
     */
    this.removeBlock = function(block) {
	if (block != null) {
	    var index = blocks.indexOf(block);
	    if (index != -1) {
		blocks = blocks.splice(index, 1);
		return true;
	    }
	    return false;
	}
	return false;
    };

    /**
     * Calculate the number of blocks that in world.
     * 
     * @return the number of blocks that in world.
     */
    this.blocksSize = function() {
	return blocks.length();
    };
};