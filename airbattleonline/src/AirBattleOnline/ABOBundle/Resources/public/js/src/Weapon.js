/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Weapon.js
 * 
 * Default class.
 * 
 * @version June 3, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var Weapon = function(weaponType, w, h, d, sp, dmg, dt) {
    
    /* Fields */
    var type = weaponType;
    var width = w;
    var height = h;
    var depth = d;
    var speed = sp;
    var damage = dmg;
    var duration = dt;
    
    /**
     * Get type of weapon.
     * 
     * @returns the type of weapon.
     */
    this.getType = function() {
	return type;
    };
    
    /**
     * Get width of weapon.
     * 
     * @returns the width of weapon.
     */
    this.getWidth = function() {
	return width;
    };
    
    /**
     * Get height of weapon.
     * 
     * @returns the height of weapon.
     */
    this.getHeight = function() {
	return height;
    };
    
    /**
     * Get depth of weapon.
     * 
     * @returns the depth of weapon.
     */
    this.getDepth = function() {
	return depth;
    };
    
    /**
     * Get speed of weapon.
     * 
     * @returns the speed of weapon.
     */
    this.getSpeed = function() {
	return speed;
    };
    
    /**
     * Get damage of weapon.
     * 
     * @returns the damage of weapon.
     */
    this.getDamage = function() {
	return damage;
    };
    
    /**
     * Get duration of weapon.
     * 
     * @returns the duration of weapon.
     */
    this.getDuration = function() {
	return duration;
    };
};