/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * World.js
 * 
 * Abstract class.
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var World = function(p, w, h, d, t) {
    // Prevent this class is instantiated as it is abstract.
    if (this.constructor === World) {
	throw new Error("Abstract class may not be instanciated.");
	return;
    }

    /* Fields */
    var width = w;
    var height = h;
    var depth = d;
    var time = t;
    // Objects.
    var player = null;
    var enemies = [];
    var projectiles = [];
    var items = [];
    var blocks = [];
    var weapons = [];
    // Background stars
    var starSystem = null;

    /**
     * Update player.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updatePlayer = function(delta) {
	player.position.add(player.velocity);
	player.increaseTime(delta);
	// Check if player is explode.
	if (player.getState() == ShipState.EXPLODE) {
	    player.decreaseExplodeCD(delta);
	    if (player.getExplodeCD() == 0) {
		player.setState(ShipState.RESPAWN);
		player.addRespawnCD();
		player.reset();
	    }
	}
	// Check if player is respawned
	else if (player.getState() == ShipState.RESPAWN) {
	    player.decreaseRespawnCD(delta);
	    if (player.getRespawnCD() == 0) {
		player.setState(ShipState.INNACTIVE);
		player.velocity.x = 0;
		player.velocity.y = 0;
		player.velocity.z = 0;
	    } else {
		if (player.velocity.x == 0) {
		    var x = THREE.Math.randInt(1, 15);
		    var y = THREE.Math.randInt(1, 15);
		    var z = THREE.Math.randInt(1, 15);
		    x = THREE.Math.randInt(0, 1) ? x : -x;
		    y = THREE.Math.randInt(0, 1) ? y : -y;
		    z = THREE.Math.randInt(0, 1) ? z : -z;
		    player.velocity.x = x;
		    player.velocity.y = y;
		    player.velocity.z = z;
		}
	    }
	}
	// player receibe damage.
	else if (player.getState() == ShipState.DAMAGE) {
	    player.push(delta);
	}
	// Player still alive.
	else {
	    // Decrease over heating if player have.
	    if (player.getOverheating() >= 0) {
		player.decreaseOverheating();
	    }
	    // Check collision.
	    this.checkCollisions(player);
	    // Check if player still alive
	    if (player.getState() == ShipState.DEAD) {
		player.setState(ShipState.EXPLODE);
		player.addExplodeCD();
	    }
	}
	this.worldBounds(player);
	// Send update player
	this.sendUpdatePlayer(player);
    };

    /**
     * Update enemies.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updateEnemies = function(delta) {
	// TODO
    };

    /**
     * Update projectiles.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updateProjectiles = function(delta) {
	for (var i = 0; i < projectiles.length; i++) {
	    var projectile = projectiles[i];
	    projectile.position.add(projectile.velocity);
	    projectile.speedUp();
	    // Check if projectile is over.
	    projectile.isOver(delta);
	    // World bounds of projectile.
	    this.worldBounds(projectile);
	    // Check collision
	    this.checkCollisions(projectile);
	    // Send projectile to update if projectile is of player.
	    if (projectile.getPlayer() == player) {
		this.sendUpdateProjectile(projectile);
	    }
	}
    };

    /**
     * Update items.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updateItems = function(delta) {
	// TODO
    };

    /**
     * Update blocks.
     * 
     * @param {Object}
     *                delta is the time elapsed.
     */
    this.updateBlocks = function(delta) {
	// TODO
    };

    this.updateTime = function(delta) {
	time = time - delta < 0 ? 0 : time - delta;
    };

    /**
     * Check collision.
     */
    this.checkCollisions = function(body) {
	if (body instanceof Ship) {
	    // Check collisions by blocks
	    // TODO
	    // Check collisions by items
	    // TODO
	    // CHeck collisions by projectiles
	    for (var i = 0; i < projectiles.length; i++) {
		var projectile = projectiles[i];
		if (projectile.getPlayer() != player) {
		    if (body.getState() != ShipState.DEAD
			    && body.getState() != ShipState.EXPLODE
			    && body.getState() != ShipState.RESPAWN
			    && body.getState() != ShipState.DAMAGE) {
			if (body.position.distanceTo(projectile.position) < body
				.getWidth()) {
			    body.removeLife(projectile.getDamage());
			    gsc.getScene().remove(projectile.getMesh());
			    this.removeProjectile(projectile);
			}
		    }
		}
	    }
	    // Check collisions by enemies
	    for (var i = 0; i < enemies.length; i++) {
		var enemy = enemies[i];
		if (enemy.getState() != ShipState.DEAD
			&& enemy.getState() != ShipState.EXPLODE
			&& enemy.getState() != ShipState.RESPAWN
			&& enemy.getState() != ShipState.DAMAGE) {
		    if (body.position.distanceTo(enemy.position) < body
			    .getWidth()) {
			body.removeLife(1);
			body.damage(enemy.velocity);
		    }
		}
	    }
	}
	if (body instanceof Projectile) {
	    if (body.getPlayer() == player) {
		// Check collisions by blocks
		// TODO
		// Check collisions by items
		// TODO
		// Check collisions by projectiles
		for (var i = 0; i < projectiles.length; i++) {
		    var projectile = projectiles[i];
		    if (body != projectile) {
			if (body.position.distanceTo(projectile.position) <= body
				.getWidth()) {
			    gsc.getScene().remove(projectile.getMesh());
			    gsc.getScene().remove(body.getMesh());
			    this.removeProjectile(projectile);
			    this.removeProjectile(body);
			}
		    }

		}
		// Check collisions by enemies
		for (var i = 0; i < enemies.length; i++) {
		    var enemy = enemies[i];
		    if (enemy.getState() != ShipState.DEAD
			    && enemy.getState() != ShipState.EXPLODE
			    && enemy.getState() != ShipState.RESPAWN
			    && enemy.getState() != ShipState.DAMAGE) {
			if (enemy.position.distanceTo(body.position) < enemy
				.getWidth()) {
			    // Remove life enemy
			    enemy.removeLife(body.getDamage());
			    if (enemy.getLife() == 0) {
				player.addScore(1);
			    }
			    // Delete projectile.
			    gsc.getScene().remove(body.getMesh());
			    this.removeProjectile(body);
			}
		    }
		}
	    }
	}
    };

    /**
     * Limits of world.
     * 
     * @param body
     *                is a body.
     */
    this.worldBounds = function(body) {
	// Limits screen
	while (body.position.x < 0)
	    body.position.x += width;
	while (body.position.y < 0)
	    body.position.y += height;
	while (body.position.z < 0)
	    body.position.z += depth;
	body.position.x = body.position.x % width;
	body.position.y = body.position.y % height;
	body.position.z = body.position.z % depth;
    };

    // -----------------------------------------
    // Management of Associations
    // -----------------------------------------
    /**
     * Get star system.
     * 
     * @return the star system.
     */
    this.getStarSystem = function() {
	return starSystem;
    };

    /**
     * Set the star system
     * 
     * @param newStarSystem
     *                is the new star system.
     */
    this.setStarSystem = function(newStarSystem) {
	starSystem = newStarSystem;
    };

    /**
     * Get player.
     * 
     * @return the player
     */
    this.getPlayer = function() {
	return player;
    };

    /**
     * Set player
     * 
     * @param newPlayer
     *                is the new player to add.
     * @return true if player has changed, false otherwise.
     */
    this.setPlayer = function(newPlayer) {
	if (player == newPlayer)
	    return false;
	var oldPlayer = player;
	player = newPlayer;
	if (oldPlayer != null) {
	    oldPlayer.setWorld(null);
	}
	if (player != null) {
	    player.setWorld(this);
	}
	return true;
    };
    this.setPlayer(p);
    player.position.x = THREE.Math.randInt(0, width);
    player.position.y = THREE.Math.randInt(0, height);
    player.position.z = THREE.Math.randInt(0, depth);

    /**
     * Get enemies.
     * 
     * @return the enemies.
     */
    this.getEnemies = function() {
	return enemies;
    };

    /**
     * Add enemy.
     * 
     * @param enemy
     *                is the enemy to add.
     * @return true if enemy has be added, false otherwise.
     */
    this.addEnemy = function(enemy) {
	if (enemy != null) {
	    enemies.push(enemy);
	    return true;
	}
	return false;
    };

    /**
     * Remove a specific enemy.
     * 
     * @param enemy
     *                is the enemy to remove.
     * @return true if enemy has be removed, false otherwise.
     */
    this.removeEnemy = function(enemy) {
	if (enemy != null) {
	    var index = enemies.indexOf(enemy);
	    if (index != -1) {
		if (enemies.length == 1)
		    enemies = [];
		else
		    enemies = enemies.splice(index, 1);
		return true;
	    }
	    return false;
	}
	return false;
    };

    /**
     * Calculate the number of enemies that in world.
     * 
     * @return the number of enemies that in world.
     */
    this.enemiesSize = function() {
	return enemies.length;
    };

    /**
     * Get items.
     * 
     * @return the items.
     */
    this.getItems = function() {
	return items;
    };

    /**
     * Add item.
     * 
     * @param item
     *                is the item to add.
     * @return true if item has be added, false otherwise.
     */
    this.addItem = function(item) {
	if (item != null) {
	    items.push(item);
	    return true;
	}
	return false;
    };

    /**
     * Remove a specific item.
     * 
     * @param item
     *                is the item to remove.
     * @return true if item has be removed, false otherwise.
     */
    this.removeItem = function(item) {
	if (item != null) {
	    var index = items.indexOf(item);
	    if (index != -1) {
		if (items.length == 1)
		    items = [];
		else
		    items = items.splice(index, 1);
		return true;
	    }
	    return false;
	}
	return false;
    };

    /**
     * Calculate the number of items that in world.
     * 
     * @return the number of items that in world.
     */
    this.itemsSize = function() {
	return items.length;
    };

    /**
     * Get projectiles.
     * 
     * @return the projectiles.
     */
    this.getProjectiles = function() {
	return projectiles;
    };

    /**
     * Add projectile.
     * 
     * @param projectile
     *                is the projectile to add.
     * @return true if projectile has be added, false otherwise.
     */
    this.addProjectile = function(projectile) {
	if (projectile != null) {
	    projectiles.push(projectile);
	    return true;
	}
	return false;
    };

    /**
     * Remove a specific projectile.
     * 
     * @param projectile
     *                is the projectile to remove.
     * @return true if projectile has be removed, false otherwise.
     */
    this.removeProjectile = function(projectile) {
	if (projectile != null) {
	    var index = projectiles.indexOf(projectile);
	    if (index != -1) {
		if (projectiles.length == 1)
		    projectiles = [];
		else
		    projectiles = projectiles.splice(index, 1);
		return true;
	    }
	    return false;
	}
	return false;
    };

    /**
     * Calculate the number of enemies that in world.
     * 
     * @return the number of enemies that in world.
     */
    this.projectilesSize = function() {
	return projectiles.length;
    };

    /**
     * Get blocks.
     * 
     * @return the enemies.
     */
    this.getBlocks = function() {
	return blocks;
    };

    /**
     * Add block.
     * 
     * @param block
     *                is the block to add.
     * @return true if block has be added, false otherwise.
     */
    this.addBlock = function(block) {
	if (block != null) {
	    blocks.push(block);
	    return true;
	}
	return false;
    };

    /**
     * Remove a specific block.
     * 
     * @param block
     *                is the block to remove.
     * @return true if block has be removed, false otherwise.
     */
    this.removeBlock = function(block) {
	if (block != null) {
	    var index = blocks.indexOf(block);
	    if (index != -1) {
		blocks = blocks.splice(index, 1);
		return true;
	    }
	    return false;
	}
	return false;
    };

    /**
     * Calculate the number of blocks that in world.
     * 
     * @return the number of blocks that in world.
     */
    this.blocksSize = function() {
	return blocks.length;
    };

    /**
     * Get blocks.
     * 
     * @return the enemies.
     */
    this.getWeapons = function() {
	return weapons;
    };

    /**
     * Add weapon.
     * 
     * @param weapon
     *                is the weapon to add.
     * @return true if weapon has be added, false otherwise.
     */
    this.addWeapon = function(weapon) {
	if (weapon != null) {
	    weapons.push(weapon);
	    return true;
	}
	return false;
    };

    /**
     * Calculate the number of weapons that in world.
     * 
     * @return the number of weapons that in world.
     */
    this.WeaponsSize = function() {
	return weapon.length;
    };

    // GETTERS & SETTERS
    /**
     * Get width of world.
     * 
     * @returns the width of world.
     */
    this.getWidth = function() {
	return width;
    };

    /**
     * Get height of world.
     * 
     * @returns the height of world.
     */
    this.getHeight = function() {
	return height;
    };

    /**
     * Get depth of world.
     * 
     * @returns the depth of world.
     */
    this.getDepth = function() {
	return depth;
    };

    /**
     * Get Time.
     * 
     * @returns the time remaining.
     */
    this.getTime = function() {
	return time;
    };

    // -----------------------------------------------------------
    // SOCKET
    // -----------------------------------------------------------
    var self = this;
    var socket = null;

    /**
     * Get socket.
     * 
     * @returns a socket.
     */
    this.getSocket = function() {
	return socket;
    };

    /**
     * Define socket.
     * 
     * @param newSocket
     *                is a socket.
     */
    this.defineSocket = function(newSocket) {
	socket = newSocket;
	socket.on('addEnemies', function(data) {
	    var enemy = null;
	    if (data.entity.type == ShipType.SHIP2D_T1) {
		var entity = data.entity;
		enemy = new Ship2DT1(entity.posx, entity.posy, entity.width,
			entity.height, entity.life, entity.shield,
			entity.speed, entity.defaultWeapon);
		enemy.setId(data.id);
		self.addEnemy(enemy);
	    } else if (data.entity.type == ShipType.SHIP3D_T1) {
		var entity = data.entity;
		enemy = new Ship3DT1(entity.posx, entity.posy, entity.posz,
			entity.width, entity.height, entity.depth, entity.life,
			entity.shield, entity.speed);
		enemy.setId(data.id);
	    }
	    if (enemy != null) {
		enemy.setWorld(player.getWorld());
		self.addEnemy(enemy);
	    }
	});

	socket.on('updateEnemies', function(data) {
	    for (var i = 0; i < enemies.length; i++) {
		if (enemies[i].getId() === data.id) {
		    enemies[i].position.x = data.entity.posx;
		    enemies[i].position.y = data.entity.posy;
		    enemies[i].position.z = data.entity.posz;
		    enemies[i].setLife(data.entity.life);
		    enemies[i].setShield(data.entity.shield);
		    enemies[i].setState(data.state);
		    enemies[i].setAngleX(data.angleX);
		    enemies[i].setAngleY(data.angleY);
		    enemies[i].setAngleZ(data.angleZ);
		}
	    }
	});

	socket.on('addProjectiles', function(data) {
	    for (var i = 0; i < enemies.length; i++) {
		if (enemies[i].getId() === data.playerId) {
		    var entity = data.entity;
		    var projectile = new Projectile(enemies[i], data.posx,
			    data.posy, data.posz, data.angleX, data.angleY,
			    data.angleZ, entity.type, entity.width,
			    entity.height, entity.depth, entity.speed,
			    entity.damage, entity.duration, data.is3D);
		    projectile.setId(data.id);
		    self.addProjectile(projectile);
		    i = enemies.length;
		}
	    }
	});

	socket.on('updateProjectiles', function(data) {
	    console.log("DATA-ID: " + data.id);
	    for (var i = 0; i < projectiles.length; i++) {
		console.log(projectiles[i].getId());
		if (projectiles[i].getId() === data.id) {
		    projectiles[i].position.x = data.posx;
		    projectiles[i].position.y = data.posy;
		    projectiles[i].position.z = data.posz;
		    projectiles[i].setAngleX(data.angleX);
		    projectiles[i].setAngleY(data.angleY);
		    projectiles[i].setAngleZ(data.angleZ);
		}
	    }
	});

	socket.on('removeEnemies', function(data) {
	    for (var i = 0; i < enemies.length; i++) {
		if (enemies[i].getId() === data) {
		    gsc.getScene().remove(enemies[i].getMesh());
		    self.removeEnemy(enemies[i]);
		}
	    }
	});
	this.sendPlayer(player);
    };

    /**
     * Send player to server.
     * 
     * @param player
     *                is a player.
     */
    this.sendPlayer = function(player) {
	if (player.getId() == null) {
	    player.setId(createId());
	}
	var entity = SettingManager.DAO.getShipEntity(player.toString());
	entity.posx = player.position.x;
	entity.posy = player.position.y;
	entity.posz = player.position.z;
	entity.life = player.getLife();
	entity.shield = player.getShield();
	socket.emit('initEnemies', {
	    "id" : player.getId(),
	    "entity" : entity,
	    "state" : player.getState(),
	    "angleX" : player.getAngleX(),
	    "angleY" : player.getAngleY(),
	    "angleZ" : player.getAngleZ()
	});
    };

    /**
     * Send projectile.
     * 
     * @param projectile
     *                is a projectile.
     */
    this.sendProjectile = function(projectile) {
	if (projectile.getId() == null) {
	    projectile.setId(createId());
	}
	var entity = SettingManager.DAO.getWeaponEntity(projectile.getType());
	entity.width = projectile.getWidth();
	entity.height = projectile.getHeight();
	entity.depth = projectile.getDepth();
	entity.speed = projectile.getSpeed();
	entity.damage = projectile.getDamage();
	entity.duration = projectile.getDuration();
	socket.emit('initProjectiles', {
	    "id" : projectile.getId(),
	    "playerId" : projectile.getPlayer().getId(),
	    "posx" : projectile.position.x,
	    "posy" : projectile.position.y,
	    "posz" : projectile.position.z,
	    "entity" : entity,
	    "angleX" : projectile.getAngleX(),
	    "angleY" : projectile.getAngleY(),
	    "angleZ" : projectile.getAngleZ(),
	    "is3D" : projectile.is3D()
	});
    };

    /**
     * Send player to server to update.
     * 
     * @param player
     *                is a player to update.
     */
    this.sendUpdatePlayer = function(player) {
	var entity = SettingManager.DAO.getShipEntity(player.toString());
	entity.posx = player.position.x;
	entity.posy = player.position.y;
	entity.posz = player.position.z;
	entity.life = player.getLife();
	entity.shield = player.getShield();
	socket.emit('updateEnemies', {
	    "id" : player.getId(),
	    "entity" : entity,
	    "state" : player.getState(),
	    "angleX" : player.getAngleX(),
	    "angleY" : player.getAngleY(),
	    "angleZ" : player.getAngleZ()
	});
    };

    /**
     * Sned projectile to server.
     * 
     * @param projectile
     *                is a projectile.
     */
    this.sendUpdateProjectile = function(projectile) {
	socket.emit('updateProjectiles', {
	    "id" : projectile.getId(),
	    "posx" : projectile.position.x,
	    "posy" : projectile.position.y,
	    "posz" : projectile.position.z,
	    "angleX" : projectile.getAngleX(),
	    "angleY" : projectile.getAngleY(),
	    "angleZ" : projectile.getAngleZ()
	});
    };

    // PRIVATE METHODS
    function createId() {
	var d = new Date().getTime();
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
		function(c) {
		    var r = (d + Math.random() * 16) % 16 | 0;
		    d = Math.floor(d / 16);
		    return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
		});
	return uuid;

    }
};