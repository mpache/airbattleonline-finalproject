/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * HUD.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var HUD = function() {

    /* Fields */
    var nextScreen = null;
    // temp
    var prevLife = 0;
    var prevShield = 0;
    var prevScore = 0;
    var prevTime = 0;
    // Time to change screen.
    var timeToChangeScreen = 0;

    /**
     * Initialized hud.
     */
    (function() {
	initComponents();
	registerListeners();
    })();

    // PRIVATE METHODS
    function initComponents() {
	var life = '<div id="life">'
		+ '<input class="knob"  data-width="136" data-angleOffset="-142" data-angleArc="232"'
		+ 'data-fgColor="#1adf10" data-bgColor="#d00404" data-thickness=".25" data-readOnly="true">'
		+ '</div>';
	var shield = '<div id="shield">'
		+ '<input class="knob"  data-width="136" data-angleOffset="-142" data-angleArc="232"'
		+ 'data-fgColor="#48bceb" data-bgColor="rgba(255,255,255,0.0)" data-thickness=".25" data-readOnly="true">'
		+ '</div>';
	var overheating = '<div id="overheating">'
		+ '<input class="knob" data-width="185" data-angleOffset="-138" data-angleArc="185"'
		+ 'data-fgColor="#bd4220" data-bgColor="rgba(255,255,255,0.1)" data-thickness=".2" data-readOnly="true">'
		+ '</div>';
	var goMainMenu = '<input id="goMainMenu" style="font-size: 0.8em;" value="Go Main Menu" '
		+ 'class="alertify-button alertify-button-ok" type="button">';
	// Create main content
	var mainContent = '<div id="main-content" class="full-width clearfix to-upper">'
		+ '<div class="game-hud-avatar"><img alt="avatar" src="/bundles/abo/images/avatar.png"></div>'
		+ '<div class="game-hud-life-shield">'
		+ life
		+ shield
		+ '</div>'
		+ '<div class="game-hud-overheating"><p id="overheating-counter">0%</p>'
		+ overheating
		+ '</div><div class="game-hud-username"><p>'
		+ username
		+ '</p></div>'
		+ '<div class="game-hud-score"><p id="score">0000</p></div>'
		+ '<div class="game-hud-time"><h1 id="time">00:00</h1></div>'
		+ '<div id="gameOver" class="game-hud-gameOver hide">'
		+ '<h1>Game Over</h1><p>Your Final Score is: '
		+ '<span id="finalScore" style="color:red"></span></p>'
		+ '<div style="text-align: center;">'
		+ goMainMenu
		+ '</div><p style="font-style: italic; color: rgb(80, 80, 80);">'
		+ 'In 5 seconds you will be redirect to main menu.</p></div>'
		+ '</div>';
	jQuery("body").append(mainContent);
	jQuery(".knob").knob();
    }

    function registerListeners() {
	jQuery("#goMainMenu").click(function() {
	    gsc.create2DRenderer(MODE_DEBUG);
	    nextScreen = new MainMenu();
	});
    }

    /**
     * Update life bar.
     * 
     * @param life
     *                is the life remaining.
     */
    this.updateLife = function(life) {
	if (life != prevLife) {
	    jQuery("#life input.knob").val(life).trigger('change');
	    prevLife = life;
	}
    };

    /**
     * Update shield bar.
     * 
     * @param shield
     *                is the shield remaining
     */
    this.updateShield = function(shield) {
	if (shield != prevShield) {
	    jQuery("#shield input.knob").val(shield).trigger('change');
	    prevShield = shield;
	}
    };

    /**
     * Update overheating bar.
     * 
     * @param shield
     *                is the actual overheating
     */
    this.updateOverheating = function(overheating) {
	overheating = overheating >> 0;
	jQuery("#overheating input.knob").val(overheating).trigger('change');
	jQuery("#overheating-counter").html(overheating + "%");
    };

    /**
     * Update score.
     * 
     * @param score
     *                is the new score of player.
     */
    this.updateScore = function(score) {
	if (score != prevScore) {
	    prevScore = score;
	    if (score < 1000) {
		var l = score.toString().length;
		for (var i = 0; i < 4 - l; i++) {
		    score = "0" + score;
		}
	    }
	    jQuery("#score").html(score);
	}
    };

    /**
     * Update time.
     */
    this.updateTime = function(time) {
	if (time != prevTime) {
	    prevTime = time;
	    var minutes = time / 60 >> 0;
	    var seconds = (time - minutes * 60) >> 0;
	    minutes = minutes <= 9 ? "0" + minutes : minutes;
	    seconds = seconds <= 9 ? "0" + seconds : seconds;
	    var t = minutes + ":" + seconds;
	    jQuery("#time").html(t);
	}
    };

    /**
     * Pause game.
     */
    this.pauseGame = function() {
	// TODO
    };

    /**
     * Resume game.
     */
    this.resumeGame = function() {
	// TODO
    };

    /**
     * Game over.
     */
    this.gameOver = function(finalScore, delta) {
	timeToChangeScreen += delta;
	if (timeToChangeScreen > 0.005) {
	    gsc.create2DRenderer(MODE_DEBUG);
	    nextScreen = new MainMenu();
	}
	// Show pop up.
	jQuery("#finalScore").html(finalScore);
	jQuery("#gameOver").show();
    };

    /**
     * Resize elements of hud.
     * 
     * @param width
     *                is the new width to reescale elements.
     * @param height
     *                is the new height to reescale elements.
     */
    this.resize = function(width, height) {
	// TODO
    };

    /**
     * Dispose game.
     */
    this.dispose = function() {
	jQuery("#main-content").remove();
    };

    /**
     * Get next screen.
     */
    this.nextScreen = function() {
	return nextScreen;
    };

    /**
     * Method to control back key in mobile phones.
     */
    this.onBackPressed = function() {
	// TODO
    };
};