/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 * 		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Ship3DT1.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var Ship3DT1 = function(x, y, z, w, h, d, l, s, sp) {
    Ship.call(this, x, y, z, w, h, d, l, s, true);

    /* Constants */
    var SPEED = sp;
    var DECELERATION = 0.9;

    /* Fields */
    var shieldMesh = null;
    var targetAngleX = 0;
    var targetAngleZ = 0;

    /**
     * Speed up.
     */
    this.speedUp = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // Speed up.
	    this.setState(ShipState.SPEED_UP);
	    this.velocity.multiplyScalar(0.96);
	    this.velocity.x -= Math.sin(this.getAngleZ())
		    * Math.cos(this.getAngleX()) * SPEED;
	    this.velocity.y += Math.cos(this.getAngleZ())
		    * Math.sin(this.getAngleX()) * SPEED;
	    this.velocity.z -= Math.cos(this.getAngleX()) * SPEED;
	}
    };

    /**
     * Speed down.
     */
    this.speedDown = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // Speed down.
	    this.setState(ShipState.SPEED_DOWN);
	    this.velocity.multiplyScalar(0.96);
	    this.velocity.multiplyScalar(DECELERATION);
	}
    };

    /**
     * Rotate to up.
     */
    this.rotateUp = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // rotate up
	    this.setState(ShipState.ROTATE);
	    this.velocity.multiplyScalar(0.96);
	    targetAngleX -= THREE.Math.degToRad(5);
	    var angle = this.calculateRotation(targetAngleX, this.getAngleX());
	    this.setAngleX(angle);
	}
    };

    /**
     * Rotate to down.
     */
    this.rotateDown = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // rotate down
	    this.setState(ShipState.ROTATE);
	    this.velocity.multiplyScalar(0.96);
	    targetAngleX += THREE.Math.degToRad(5);
	    var angle = this.calculateRotation(targetAngleX, this.getAngleX());
	    this.setAngleX(angle);
	}
    };

    /**
     * Rotate to left.
     */
    this.rotateLeft = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // rotate left
	    this.setState(ShipState.ROTATE);
	    this.velocity.multiplyScalar(0.96);
	    targetAngleZ -= THREE.Math.degToRad(5);
	    var angle = this.calculateRotation(targetAngleZ, this.getAngleZ());
	    this.setAngleZ(angle);
	}
    };

    /**
     * Rotate to right.
     */
    this.rotateRight = function() {
	if (this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    // rotate right
	    this.setState(ShipState.ROTATE);
	    this.velocity.multiplyScalar(0.96);
	    targetAngleZ += THREE.Math.degToRad(5);
	    var angle = this.calculateRotation(targetAngleZ, this.getAngleZ());
	    this.setAngleZ(angle);
	}
    };

    /**
     * Shoot.
     */
    this.shoot = function(delta) {
	if (!this.isOverheating() && this.getState() != ShipState.DEAD
		&& this.getState() != ShipState.EXPLODE
		&& this.getState() != ShipState.RESPAWN
		&& this.getState() != ShipState.DAMAGE) {
	    if (this.getTime() >= 0.5) {
		this.resetTime();
		var w = this.getActualWeapon();
		var p = new Projectile(this, this.position.x, this.position.y,
			this.position.z, this.getAngleX(), this.getAngleY(),
			this.getAngleZ(), w.getType(), w.getWidth(), w
				.getHeight(), w.getDepth(), w.getSpeed(), w
				.getDamage(), w.getDuration(), true);
		p.position.x = this.position.x;
		p.position.y = this.position.y;
		p.position.z = this.position.z;
		this.getWorld().addProjectile(p);
		this.getWorld().sendProjectile(p);
		// Add overheating.
		this.increaseOverheating(w.getDamage());
	    }
	}
    };

    // GETTERS & SETTERS
    /**
     * Get shield mesh.
     * 
     * @returns a mesh.
     */
    this.getShieldMesh = function() {
	return shieldMesh;
    };

    /**
     * Set shield mesh.
     * 
     * @param newShieldMesh
     *                is the new mesh.
     */
    this.setShieldMesh = function(newShieldMesh) {
	shieldMesh = newShieldMesh;
    };

    /**
     * Get speed of ship.
     * 
     * @returns the speed of ship.
     */
    this.getSpeed = function() {
	return SPEED;
    };

    // TO STRING
    /** @Override */
    this.toString = function() {
	return ShipType.SHIP3D_T1;
    };
};

// Extends of Ship.
Ship3DT1.prototype = Object.create(Ship.prototype);
Ship3DT1.prototype.constructor = Ship3DT1;