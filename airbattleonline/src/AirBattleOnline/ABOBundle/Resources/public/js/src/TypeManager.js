/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * TypeManager.js
 * 
 * @version June 3, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */

// Block types
// TODO

// Weapon types
var weaponType = function() {};
weaponType.DEFAULT = "DEFAULT";
weaponType.MISSILE = "MISSILE";

// Ship types.
var ShipType = function() {};
ShipType.SHIP2D_T1 = "SHIP2D_T1";
ShipType.SHIP3D_T1 = "SHIP3D_T1";

// World types
var WorldType = function() {};
WorldType.WORLD_G1 = "WORLD_G1";
WorldType.WORLD_G2 = "WORLD_G2";
WorldType.WORLD_G3 = "WORLD_G3";
WorldType.WORLD_G4 = "WORLD_G4";