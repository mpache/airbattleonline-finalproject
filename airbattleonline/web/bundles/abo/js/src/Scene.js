/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Scene.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var Scene = function() {

    /**
     * Create star system in 2D.
     * 
     * @returns the object that represents star system.
     */
    this.create2DStarSystem = function(worldWidth, worldHeight) {
	var starSystem = new THREE.Object3D();
	var material = new THREE.SpriteMaterial({
	    map : THREE.ImageUtils.loadTexture("/bundles/abo/images/spikey.png"),
	    blending : THREE.AdditiveBlending
	});
	for (var i = 0; i < 300; i++) {
	    var particle = new THREE.Sprite(material);
	    particle.position.x = THREE.Math.randInt(-500, worldWidth + 500);
	    particle.position.y = THREE.Math.randInt(-500, worldHeight + 500);
	    particle.position.z = -1;
	    particle.scale.x = particle.scale.y = Math.random() * 10 + 5;
	    starSystem.add(particle);
	}
	return starSystem;
    };

    /**
     * Create skybox.
     * 
     * @param width
     *                is the width of world.
     * @param height
     *                is the height of world.
     * @param depth
     *                is the depth of world.
     * 
     * @returns object that represent skybox.
     */
    this.create3DStarSystem = function(width, height, depth) {
	// Determine url of images, name and prefix.
	var imagePrefix = "/bundles/abo/images/nebula-";
	var directions = [ "xpos", "xneg", "ypos", "yneg", "zpos", "zneg" ];
	var imageSuffix = ".png";
	// Create geometry
	var skyGeometry = new THREE.BoxGeometry(15000, 15000, 15000);
	var imageURLs = [];
	for (var i = 0; i < 6; i++)
	    imageURLs.push(imagePrefix + directions[i] + imageSuffix);
	var textureCube = THREE.ImageUtils.loadTextureCube(imageURLs);
	// Create shader world.
	var shader = THREE.ShaderLib["cube"];
	shader.uniforms["tCube"].value = textureCube;
	var skyMaterial = new THREE.ShaderMaterial({
	    fragmentShader : shader.fragmentShader,
	    vertexShader : shader.vertexShader,
	    uniforms : shader.uniforms,
	    depthWrite : false,
	    side : THREE.BackSide
	});
	var skyBox = new THREE.Mesh(skyGeometry, skyMaterial);
	skyBox.position.set(2500, 0, 0);
	return skyBox;
    };

    /**
     * Create a 2D projectile.
     * 
     * @param projectile2DType
     *                is the 2D projectile type.
     * 
     * @returns a object that represents projectile 2D
     */
    this.create2DProjectile = function(projectileType, width, height) {
	// Create player object.
	var projectile = new THREE.Object3D();

	// Create body.
	var shape = new THREE.Shape();
	shape.moveTo(-width / 2, height / 2);
	shape.lineTo(width / 2, height / 2);
	shape.lineTo(width / 2 + 5, 0);
	shape.lineTo(width / 2, -height / 2);
	shape.lineTo(-width / 2, -height / 2);
	shape.lineTo(-width / 2, height / 2);
	// Fill
	var geometry = new THREE.ShapeGeometry(shape);
	var material = new THREE.MeshBasicMaterial({
	    color : 0xffff00,
	    overdraw : true
	});
	var mesh = new THREE.Mesh(geometry, material);
	mesh.name = "projectile";
	projectile.add(mesh);

	// Return projectile
	return projectile;
    };

    /**
     * Create a 3D projectile.
     * 
     * 
     */
    this.create3DProjectile = function(projectileType, width, height, depth, fn) {
	var loader = new THREE.OBJMTLLoader();
	loader.load('/bundles/abo/models/misil.obj', '/bundles/abo/models/misil.mtl', function(object) {
	    object.scale.x = width;
	    object.scale.y = height;
	    object.scale.z = depth;
	    object.name = "misile";
	    fn(object);
	});
    };

    /**
     * Create a 2D item.
     * 
     * @param item2DType
     *                is the 2D item type.
     * 
     * @returns a object that represents item 2D
     */
    this.create2DItem = function(item2DType) {
	// TODO
    };

    /**
     * Create a 2D block.
     * 
     * @param block2DType
     *                is the 2D block type.
     * 
     * @returns a object that represents block 2D
     */
    this.create2DBlock = function(block2DType) {
	// TODO
    };
};
