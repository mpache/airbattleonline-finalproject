/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Character.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var Character = function() {

    /**
     * Create a player 2D.
     * 
     * @returns a mesh that represents a player.
     */
    this.create2DPlayer = function(characterType, width, height) {
	// Create player object.
	var player = new THREE.Object3D();

	// Create body.
	var shape = new THREE.Shape();
	shape.moveTo(-width / 2, height / 2);
	shape.lineTo(width / 2, 0);
	shape.lineTo(-width / 2, -height / 2);
	shape.lineTo(-width / 2, height / 2);
	// Fill
	var geometry = new THREE.ShapeGeometry(shape);
	var material = new THREE.MeshBasicMaterial({
	    color : 0x41AFAA,
	    overdraw : true
	});
	var mesh = new THREE.Mesh(geometry, material);
	mesh.name = "body";
	player.add(mesh);

	// Create propulsion.
	shape = new THREE.Shape();
	shape.moveTo(-45, 0);
	shape.lineTo(-29, -10);
	shape.lineTo(-25, -5);
	shape.lineTo(-25, 5);
	shape.lineTo(-29, 10);
	shape.lineTo(-45, 0);
	// Stroke
	geometry = shape.createPointsGeometry();
	material = new THREE.LineBasicMaterial({
	    linewidth : 2,
	    color : 0xFF0000,
	    transparent : true
	});
	var line = new THREE.Line(geometry, material);
	line.name = "propulsion";
	player.add(line);

	// Create shield
	shape = new THREE.Shape();
	shape.moveTo(25, 0);
	shape.absarc(-5, 0, 30, 0, Math.PI * 2, false);
	// Stroke
	geometry = shape.createPointsGeometry();
	material = new THREE.LineBasicMaterial({
	    linewidth : 2,
	    color : 0xbd4220,
	    transparent : true,
	    opacity : 0.5,
	});
	var line = new THREE.Line(geometry, material);
	line.name = "shield";
	player.add(line);
	
	// Create explosion
	shape = new THREE.Shape();
	shape.moveTo(5, 0);
	shape.absarc(-5, 0, 10, 0, Math.PI * 2, false);
	// Fill
	var geometry = new THREE.ShapeGeometry(shape);
	var material = new THREE.MeshBasicMaterial({
	    color : 0xFF0000,
	    overdraw : true
	});
	var mesh = new THREE.Mesh(geometry, material);
	mesh.name = "explosion";
	player.add(mesh);
	
	// return player
	return player;
    };

    /**
     * Create a player 3D.
     * 
     * @param playerType
     *                is the player type.
     * @param fn
     *                is a function to send object when loader recived.
     * 
     * @returns a object that represents a player.
     */
    this.create3DPlayer = function(playerType, width, height, depth, fn) {
	var loader = new THREE.OBJMTLLoader();
	loader.load('/bundles/abo/models/avion.obj', '/bundles/abo/models/avion.mtl', function(object) {
	    object.scale.x = width;
	    object.scale.y = height;
	    object.scale.z = depth;
	    object.name = "body";
	    fn(object);
	});
    };
    
    this.create3DGlow = function(playerType, camera, scale) {
	 var customMaterial = new THREE.ShaderMaterial({
	     uniforms: {
		 "c": { type: "f", value: 1.0 },
		 "p":   { type: "f", value: 2.0 },
		 glowColor: {
		     type: "c", value: new THREE.Color(0xbd4220) },
		     viewVector: { type: "v3", value: camera.position }
		 },
		 vertexShader:   document.getElementById( 'vertexShader'   ).textContent,
		 fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
		 side: THREE.FrontSide,
		 blending: THREE.AdditiveBlending,
		 transparent: true
	});
	var sphereGeom = new THREE.SphereGeometry(100, 32, 16);
	// Return glow.
	var glow =  new THREE.Mesh( sphereGeom, customMaterial );
	glow.scale.multiplyScalar(scale);
	glow.name = "shield";
	return glow;
    };
    
    /**
     * Create a enemy 2d.
     * 
     * @returns a object that represents enemy in 2d.
     */
    this.create2DEnemy = function() {
	// Create player object.
	var enemy = new THREE.Object3D();

	// Create body.
	var shape = new THREE.Shape();
	shape.moveTo(-20, 15);
	shape.lineTo(20, 0);
	shape.lineTo(-20, -15);
	shape.lineTo(-20, 15);
	// Stroke
	geometry = shape.createPointsGeometry();
	material = new THREE.LineBasicMaterial({
	    linewidth : 2,
	    color : 0xFFFFFF,
	    transparent : true
	});
	var line = new THREE.Line(geometry, material);
	line.name = "body";
	enemy.add(line);

	// Create propulsion.
	shape = new THREE.Shape();
	shape.moveTo(-45, 0);
	shape.lineTo(-29, -10);
	shape.lineTo(-25, -5);
	shape.lineTo(-25, 5);
	shape.lineTo(-29, 10);
	shape.lineTo(-45, 0);
	// Stroke
	geometry = shape.createPointsGeometry();
	material = new THREE.LineBasicMaterial({
	    linewidth : 2,
	    color : 0xFF0000,
	    transparent : true
	});
	var line = new THREE.Line(geometry, material);
	line.name = "propulsion";
	enemy.add(line);

	// Create shield
	shape = new THREE.Shape();
	shape.moveTo(25, 0);
	shape.absarc(-5, 0, 30, 0, Math.PI * 2, false);
	// Stroke
	geometry = shape.createPointsGeometry();
	material = new THREE.LineBasicMaterial({
	    linewidth : 2,
	    color : 0xbd4220,
	    transparent : true,
	    opacity : 0.5,
	});
	var line = new THREE.Line(geometry, material);
	line.name = "shield";
	enemy.add(line);

	// Create explosion
	shape = new THREE.Shape();
	shape.moveTo(5, 0);
	shape.absarc(-5, 0, 10, 0, Math.PI * 2, false);
	// Fill
	var geometry = new THREE.ShapeGeometry(shape);
	var material = new THREE.MeshBasicMaterial({
	    color : 0xFF0000,
	    overdraw : true
	});
	var mesh = new THREE.Mesh(geometry, material);
	mesh.name = "explosion";
	enemy.add(mesh);
	
	// return player
	return enemy;
    };
    
    /**
     * Create a enemy 3d.
     *
     * @returns a object that represents enemy in 3d.
     */
    this.create3DEnemy = function(enemyType, width, height, depth, fn) {
	var loader = new THREE.OBJMTLLoader();
	loader.load('/bundles/abo/models/avion.obj', 'models/avion.mtl', function(object) {
	    object.scale.x = width;
	    object.scale.y = height;
	    object.scale.z = depth;
	    object.name = "body";
	    fn(object);
	});
    };
};