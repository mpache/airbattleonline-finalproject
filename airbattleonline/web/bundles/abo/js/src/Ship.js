/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 * 		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Ship.js
 * 
 * Abstract class.
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var Ship = function(x, y, z, w, h, d, l, s, is3D) {
    // Prevent this class is instantiated as it is abstract.
    if (this.constructor === Ship) {
	throw new Error("Abstract class may not be instanciated.");
	return;
    }
    DynamicBody.call(this, x, y, z, w, h, d, is3D);

    /* Constants */
    var IMMUNITY_TIME = 1; // time in seconds
    var EXPLODE_TIME = 2; // time in seconds
    var RESPAWN_TIME = 5; // time in seconds
    var DECREASE_OVERHEATING = 0.1; // units;

    /* Fields */
    var world = null;

    // Count downs.
    var immunityCD = IMMUNITY_TIME;
    var explodeCD = 0;
    var respawnCD = 0;

    // id, state and mesh
    var id = null;
    var state = ShipState.INNACTIVE;
    var mesh = null;
    // life
    var maxLife = l;
    var life = maxLife;
    // Shield
    var maxShield = s;
    var shield = maxShield;
    // Overheating.
    var maxOverheating = 100;
    var overheating = 0;
    var isOverheating = false;
    // Weapons
    var actualWeapon = null;
    var weapons = [];
    // score
    var score = 0;

    // Angle to rotate ship.
    var angleX = 0;
    var angleY = 0;
    var angleZ = 0;

    // time to control any things
    var time = 0;

    /**
     * Get world associated to this ship.
     */
    this.getWorld = function() {
	return world;
    };

    /**
     * Set specific world.
     */
    this.setWorld = function(newWorld) {
	world = newWorld;
    };

    /**
     * Get the id of ship.
     * 
     * @returns the id of ship.
     */
    this.getId = function() {
	return id;
    };

    /**
     * Set the id of this ship
     * 
     * @param newId
     *                is the new id of ship.
     */
    this.setId = function(newId) {
	id = newId;
    };

    /**
     * Get state of ship.
     * 
     * @returns the state of ship.
     */
    this.getState = function() {
	return state;
    };

    /**
     * Change state of ship.
     * 
     * @param newState
     *                is the new state of ship.
     */
    this.setState = function(newState) {
	state = newState;
    };

    /**
     * Get mesh that represents this ship.
     * 
     * @returns the mesh of this sip.
     */
    this.getMesh = function() {
	return mesh;
    };

    /**
     * Set the mesh of this ship
     * 
     * @param newMesh
     *                is the new mesh of ship.
     */
    this.setMesh = function(newMesh) {
	mesh = newMesh;
    };

    /**
     * Get maximum life of ship
     * 
     * @returns the maximum life of ship.
     */
    this.getMaxLife = function() {
	return maxLife;
    };

    /**
     * Get life of ship.
     * 
     * @returns the life remaining of ship.
     */
    this.getLife = function() {
	return life;
    };

    /**
     * Set life.
     * 
     * @param newLife
     *                is the new life.
     */
    this.setLife = function(newLife) {
	life = newLife;
    };

    /**
     * Add life to ship.
     * 
     * @param lifeToAdd
     *                is the life to add to this ship.
     */
    this.addLife = function(lifeToAdd) {
	life = life + lifeToAdd >= maxLife ? maxLife : life + lifeToAdd;
    };

    /**
     * Remove life to ship
     * 
     * @param lifeToRemove
     *                is the life to remove to this ship.
     */
    this.removeLife = function(lifeToRemove) {
	if (shield > 0) {
	    var tmp = shield;
	    shield = shield - lifeToRemove >= 0 ? shield - lifeToRemove : 0;
	    lifeToRemove = tmp - lifeToRemove >= 0 ? 0 : lifeToRemove - shield;
	}
	life = life - lifeToRemove > 0 ? life - lifeToRemove : 0;
	state = life == 0 ? ShipState.DEAD : state;
    };

    /**
     * Get max shield.
     * 
     * @returns the maximum max shield.
     */
    this.getMaxShield = function() {
	return maxShield;
    };

    /**
     * Get shield of ship.
     */
    this.getShield = function() {
	return shield;
    };

    /**
     * Set shield.
     * 
     * @param newShield
     *                is the new Shield.
     */
    this.setShield = function(newShield) {
	shield = newShield;
    };

    /**
     * Add shield to this ship-
     * 
     * @param shieldToAdd
     *                is the shield to add to this ship.
     */
    this.addShield = function(shieldToAdd) {
	shield = shield + shieldToAdd >= maxShield ? maxShield : shield
		+ shieldToAdd;
    };

    /**
     * Get maximum overheating.
     * 
     * @returns the maximum overheating.
     */
    this.getMaxOverheating = function() {
	return maxOverheating;
    };

    /**
     * Get overheating of ship.
     * 
     * @returns the actual overheating of this ship.
     */
    this.getOverheating = function() {
	return overheating;
    };

    /**
     * Increase overheating.
     * 
     * @param units
     *                is the number of units to add to overheating.
     */
    this.increaseOverheating = function(units) {
	overheating = overheating + units > maxOverheating ? maxOverheating
		: overheating + units;
	if (!isOverheating && overheating == maxOverheating) {
	    isOverheating = true;
	}
    };

    /**
     * Decrease overheating.
     */
    this.decreaseOverheating = function() {
	overheating = overheating - DECREASE_OVERHEATING < 0 ? 0 : overheating
		- DECREASE_OVERHEATING;
	if (isOverheating && overheating == 0) {
	    isOverheating = false;
	}
    };

    this.isOverheating = function() {
	return isOverheating;
    };

    /**
     * Get actual score.
     * 
     * @returns the actual score.
     */
    this.getScore = function() {
	return score;
    };

    /**
     * Add score.
     * 
     * @param scoreToAdd
     *                is the score to add to actual score.
     */
    this.addScore = function(scoreToAdd) {
	score += scoreToAdd;
    };

    /**
     * Add score.
     * 
     * @param scoreToRemove
     *                is the score to add to actual score.
     */
    this.removeScore = function(scoreToRemove) {
	score = score - scoreToRemove < 0 ? 0 : score - scoreToRemove;
    };

    /**
     * Get actual weapon, by default is null (initial weapon).
     * 
     * @returns the actual weapon.
     */
    this.getActualWeapon = function() {
	return actualWeapon;
    };

    /**
     * Get next weapon.
     * 
     * @returns the next weapon.
     */
    this.nextWeapon = function() {
	var i = weapons.indexOf(actualWeapon);
	if (i != -1) {
	    var w = weapons[i + 1];
	    actualWeapon = typeof w !== "undefined" ? w : actualWeapon;
	}
    };

    /**
     * Get previous weapon.
     * 
     * @returns the previous weapon.
     */
    this.prevWeapon = function() {
	var i = weapons.indexOf(actualWeapon);
	if (i != -1) {
	    var w = weapons[i - 1];
	    actualWeapon = typeof w !== "undefined" ? w : actualWeapon;
	}
    };

    /**
     * Add weapon.
     * 
     * @param weapon
     *                is the weapon to add.
     * @return true if weapon has be added, false otherwise.
     */
    this.addWeapon = function(weapon) {
	if (weapon != null) {
	    weapons.push(weapon);
	    if (actualWeapon == null) {
		actualWeapon = weapon;
	    }
	    return true;
	}
	return false;
    };

    /**
     * Remove a specific weapon.
     * 
     * @param weapon
     *                is the weapon to remove.
     * @return true if weapon has be removed, false otherwise.
     */
    this.removeWeapon = function(weapon) {
	if (weapon != null) {
	    var index = weapons.indexOf(weapon);
	    if (index != -1) {
		if (weapons.length == 1)
		    weapons = [];
		else
		    weapons = weapons.splice(index, 1);
		return true;
	    }
	    return false;
	}
	return false;
    };

    /**
     * Get invincible count down.
     * 
     * @returns the actual invicnible count down.
     */
    this.getImmunityCD = function() {
	return immunityCD;
    };

    /**
     * Add immunity to this ship.
     */
    this.addImmunityCD = function() {
	immunityCD = IMMUNITY_TIME;
    };

    /**
     * Decrease immunity.
     * 
     * @param delta
     *                is the time elapsed between frames.
     */
    this.decreaseImmunityCD = function(delta) {
	immunityCD = immunityCD - delta > 0 ? immunityCD - delta : 0;
    };

    /**
     * Get explode count down.
     * 
     * @returns the explode count down.
     */
    this.getExplodeCD = function() {
	return explodeCD;
    };

    /**
     * Add explode time.
     */
    this.addExplodeCD = function() {
	explodeCD = EXPLODE_TIME;
    };

    /**
     * Decrease explode count down.
     * 
     * @param delta
     *                is the time elapsed between frames.
     */
    this.decreaseExplodeCD = function(delta) {
	explodeCD = explodeCD - delta > 0 ? explodeCD - delta : 0;
    };

    /**
     * Get respawn count down.
     * 
     * @returns the respawn count down.
     */
    this.getRespawnCD = function() {
	return respawnCD;
    };

    /**
     * Add respawn time.
     */
    this.addRespawnCD = function() {
	respawnCD = RESPAWN_TIME;
    };

    /**
     * Decrease respawn count down.
     * 
     * @param delta
     *                is the time elapsed between frames.
     */
    this.decreaseRespawnCD = function(delta) {
	respawnCD = respawnCD - delta > 0 ? respawnCD - delta : 0;
    };

    /**
     * Damage.
     */
    this.damage = function(velocity) {
	state = ShipState.DAMAGE;
	this.velocity.x = THREE.Math.randInt(0, 1) ? this.velocity.x - 2
		: this.velocity.x + 2;
	this.velocity.y = THREE.Math.randInt(0, 1) ? this.velocity.y - 2
		: this.velocity.y + 2;
	this.velocity.z = THREE.Math.randInt(0, 1) ? this.velocity.z - 2
		: this.velocity.z + 2;

    };

    /**
     * Push ship
     */
    this.push = function(delta) {
	time += delta;
	if (time >= 1) {
	    time = 0;
	    state = ShipState.INNACTIVE;
	}
	;
    };

    /**
     * Reset Fields of ship.
     */
    this.reset = function() {
	// Reset life shield and overheating.
	life = maxLife;
	shield = 0;
	overheating = 0;
	// Reset default weapons and weapons.
	var tmpW = weapons[0];
	actualWeapon = tmpW;
	weapons = [ tmpW ];
	// Reset velocity
	this.velocity.x = 0;
	this.velocity.y = 0;
	this.velocity.z = 0;
    };

    /**
     * Get angle in x axis of this ship.
     * 
     * @returns the angle in x axis of this ship.
     */
    this.getAngleX = function() {
	return angleX;
    };

    /**
     * Set angle in x axis of this ship.
     */
    this.setAngleX = function(newAngleX) {
	angleX = newAngleX;
    };

    /**
     * Get angle in y axis of this ship.
     * 
     * @returns the angle in y axis of this ship.
     */
    this.getAngleY = function() {
	return angleY;
    };

    /**
     * Set angle in y axis of this ship.
     */
    this.setAngleY = function(newAngleY) {
	angleY = newAngleY;
    };

    /**
     * Get angle in z axis of this ship.
     * 
     * @returns the angle in z axis of this ship.
     */
    this.getAngleZ = function() {
	return angleZ;
    };

    /**
     * Set angle in z axis of this ship.
     */
    this.setAngleZ = function(newAngleZ) {
	angleZ = newAngleZ;
    };

    /**
     * Get time
     * 
     * @returns float.
     */
    this.getTime = function() {
	return time;
    };

    /**
     * Increase time.
     * 
     * @param delta
     *                is the time elapsed.
     */
    this.increaseTime = function(delta) {
	time += delta;
    };

    /**
     * Put time to 0.
     */
    this.resetTime = function() {
	time = 0;
    };

    /**
     * Calculate rotation.
     */
    this.calculateRotation = function(targetAngle, angle) {
	if (targetAngle > angle + Math.PI)
	    targetAngle -= Math.PI * 2;
	if (targetAngle < angle - Math.PI)
	    targetAngle += Math.PI * 2;
	angle += (targetAngle - angle) * 0.4;
	return angle;
    };
};

// Extends of DynamicBody.
Ship.prototype = Object.create(DynamicBody.prototype);
Ship.prototype.constructor = Ship;
