/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * UI2DGameLoop.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var UI2DGameLoop = function(controller) {

    /* Fields */
    // Elements of this ui
    var sceneUI = new Scene();
    var hud = new HUD();
    var characters = new Character();

    // Scene and camera
    var scene = gsc.getScene();
    var camera = gsc.getCamera();

    // input processor
    var input = new InputGameLoop(controller);

    /**
     * Update life of player.
     */
    this.updateLife = function(life) {
	hud.updateLife(life);
    };

    /**
     * Update shield of player.
     */
    this.updateShield = function(shield) {
	hud.updateShield(shield);
    };

    /**
     * Update overheating of this player.
     */
    this.updateOverheating = function(overheating) {
	hud.updateOverheating(overheating);
    };

    /**
     * Update score of player.
     */
    this.updateScore = function(score) {
	hud.updateScore(score);
    };

    /**
     * Update time remaining if exists.
     */
    this.updateTime = function(time) {
	hud.updateTime(time);
    };

    /**
     * Render player.
     */
    this.renderPlayer = function(playerType, player, delta) {
	input.checkInput();
	// render player
	var mesh = player.getMesh();
	if (mesh == null) {
	    mesh = characters.create2DPlayer(playerType, player.getWidth(),
		    player.getHeight());
	    player.setMesh(mesh);
	    scene.add(mesh);
	}
	// Check if player is explode
	if (player.getState() == ShipState.EXPLODE) {
	    mesh.getObjectByName("explosion").visible = true;
	    if (mesh.getObjectByName("explosion").scale.x >= 2.5
		    || !mesh.getObjectByName("body").visible) {
		if (mesh.getObjectByName("explosion").scale.x >= 0) {
		    mesh.getObjectByName("explosion").scale.x -= 0.1;
		    mesh.getObjectByName("explosion").scale.y -= 0.1;
		} else {
		    mesh.getObjectByName("explosion").visible = false;
		}
		mesh.getObjectByName("body").visible = false;
		mesh.getObjectByName("shield").visible = false;
		mesh.getObjectByName("propulsion").visible = false;
	    } else {
		mesh.getObjectByName("explosion").scale.x += 0.1;
		mesh.getObjectByName("explosion").scale.y += 0.1;
	    }
	}
	// Check if player is respawned
	else if (player.getState() == ShipState.RESPAWN) {
	    mesh.getObjectByName("explosion").scale.x = 1;
	    mesh.getObjectByName("explosion").scale.y = 1;
	    mesh.visible = false;
	}
	// Player is alive
	else {
	    if (mesh.visible == false) {
		mesh.visible = true;
		mesh.getObjectByName("body").visible = true;
	    }
	    // Render shield if player have.
	    mesh.getObjectByName("shield").visible = player.getShield() > 0 ? true
		    : false;
	    // Render propulsion if player accelerate.
	    mesh.getObjectByName("propulsion").visible = player.getState() == ShipState.SPEED_UP ? true
		    : false;
	    // Hide explosion
	    mesh.getObjectByName("explosion").visible = false;

	    // Create zoom effect.
	    if (KeyboardTracker.isKeyDown(KeyboardTracker.UP)
		    && camera.top < 400) {
		camera.top = (1.01 * camera.top);
		camera.bottom = (1.01 * camera.bottom);
		camera.left = (1.01 * camera.left);
		camera.right = (1.01 * camera.right);
	    } else if (camera.top > 300) {
		camera.top = (0.99 * camera.top);
		camera.bottom = (0.99 * camera.bottom);
		camera.left = (0.99 * camera.left);
		camera.right = (0.99 * camera.right);
	    }
	    camera.updateProjectionMatrix();
	}
	// Change position of player
	mesh.position.x = player.position.x;
	mesh.position.y = player.position.y;
	mesh.rotation.z = player.getAngleX();

	// update camera
	camera.position.x = player.position.x;
	camera.position.y = player.position.y;
    };

    /**
     * Render enemy.
     */
    this.renderEnemy = function(enemyType, enemy, delta) {
	// render Enemy
	var mesh = enemy.getMesh();
	if (mesh == null) {
	    mesh = characters.create2DEnemy(enemyType, enemy.getWidth(), enemy
		    .getHeight());
	    enemy.setMesh(mesh);
	    scene.add(mesh);
	}
	// Check if Enemy is explode
	if (enemy.getState() == ShipState.EXPLODE) {
	    mesh.getObjectByName("explosion").visible = true;
	    if (mesh.getObjectByName("explosion").scale.x >= 2.5
		    || !mesh.getObjectByName("body").visible) {
		if (mesh.getObjectByName("explosion").scale.x >= 0) {
		    mesh.getObjectByName("explosion").scale.x -= 0.1;
		    mesh.getObjectByName("explosion").scale.y -= 0.1;
		} else {
		    mesh.getObjectByName("explosion").visible = false;
		}
		mesh.getObjectByName("body").visible = false;
		mesh.getObjectByName("shield").visible = false;
		mesh.getObjectByName("propulsion").visible = false;
	    } else {
		mesh.getObjectByName("explosion").scale.x += 0.1;
		mesh.getObjectByName("explosion").scale.y += 0.1;
	    }
	}
	// Check if Enemy is respawned
	else if (enemy.getState() == ShipState.RESPAWN) {
	    mesh.getObjectByName("explosion").scale.x = 1;
	    mesh.getObjectByName("explosion").scale.y = 1;
	    mesh.visible = false;
	}
	// Enemy is alive
	else {
	    if (mesh.visible == false) {
		mesh.visible = true;
		mesh.getObjectByName("body").visible = true;
	    }
	    // Render shield if enemy have.
	    mesh.getObjectByName("shield").visible = enemy.getShield() > 0 ? true
		    : false;
	    // Render propulsion if enemy accelerate.
	    mesh.getObjectByName("propulsion").visible = enemy.getState() == ShipState.SPEED_UP ? true
		    : false;
	    // Hide explosion
	    mesh.getObjectByName("explosion").visible = false;
	}
	// Change position of enemy
	mesh.position.x = enemy.position.x;
	mesh.position.y = enemy.position.y;
	mesh.rotation.z = enemy.getAngleX();
    };

    /**
     * Render projectiles.
     */
    this.renderProjectile = function(projectile, delta) {
	// render projectile
	var mesh = projectile.getMesh();
	if (mesh == null) {
	    mesh = sceneUI.create2DProjectile(projectile,
		    projectile.getWidth(), projectile.getHeight());
	    projectile.setMesh(mesh);
	    scene.add(mesh);
	}
	// Change position of projectile
	mesh.position.x = projectile.position.x;
	mesh.position.y = projectile.position.y;
	mesh.rotation.z = projectile.getAngleX();
    };

    /**
     * Render items.
     */
    this.renderItem = function(item, delta) {
	// TODO
    };

    /**
     * Render blocks.
     */
    this.renderBlock = function(block, delta) {
	// TODO
    };

    /**
     * Render background.
     */
    this.renderBackground = function(world, delta) {
	if (world.getStarSystem() == null) {
	    var starSystem = sceneUI.create2DStarSystem(world.getWidth(), world
		    .getHeight());
	    world.setStarSystem(starSystem);
	    scene.add(starSystem);
	}
    };

    /**
     * Pause game.
     */
    this.pauseGame = function() {
	hud.pauseGame();
    };

    /**
     * Resume game.
     */
    this.resumeGame = function() {
	hud.resumeGame();
    };

    /**
     * Game over.
     */
    this.gameOver = function(finalScore, delta) {
	hud.gameOver(finalScore, delta);
    };

    /**
     * Resize game.
     */
    this.resize = function(width, height) {
	hud.resize(width, height);
    };

    /**
     * Dispose game.
     */
    this.dispose = function() {
	hud.dispose();
    };

    /**
     * Get next screen.
     */
    this.nextScreen = function() {
	return hud.nextScreen();
    };

    /**
     * Control back key in moblie phones.
     */
    this.onBackPressed = function() {
	return hud.onBackPressed();
    };
};