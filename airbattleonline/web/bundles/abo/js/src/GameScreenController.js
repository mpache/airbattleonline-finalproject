/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 * 		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline2d.
 *
 * AirBattleOnline2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline2d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * GameScreenController.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var GameScreenController = function() {

    /* Fields */
    var currentScreen = null;
    var nextScreen = null;
    // Fps logger
    var log = null;
    // Clock to create delta time.
    var clock = new THREE.Clock();
    // Renderer,scene, and camera
    var renderer = null;
    var scene = null;
    var camera = null;

    /**
     * Create a 2D render.
     */
    this.create2DRenderer = function(modeDebug) {
	if (renderer != null)
	    jQuery(renderer.domElement).remove();
	// Renderer
	renderer = new THREE.CanvasRenderer();
	renderer.setSize(innerWidth, innerHeight);
	document.body.appendChild(renderer.domElement);
	// Scene and camera
	scene = new THREE.Scene();
	camera = new THREE.OrthographicCamera(innerWidth / -2, innerWidth / 2,
		innerHeight / 2, innerHeight / -2, 1, 1000);
	scene.add(camera);
	camera.position.set(0, 0, 1);
	camera.lookAt(scene.position);
	// Logger
	if (modeDebug) {
	    log = new Stats();
	    log.domElement.style.position = 'absolute';
	    log.domElement.style.bottom = '0';
	    log.domElement.style.zIndex = 9999;
	    document.body.appendChild(log.domElement);
	}

	if (arguments.length == 2) {
	    currentScreen = new MainMenu();
	}
    };

    /**
     * Create 3D render.
     */
    this.create3DRenderer = function(modeDebug) {
	if (renderer != null)
	    jQuery(renderer.domElement).remove();
	// Renderer
	renderer = new THREE.WebGLRenderer({
	    antialias : true
	});
	renderer.setSize(innerWidth, innerHeight);
	document.body.appendChild(renderer.domElement);
	// Scene and camera
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(45, innerWidth / innerHeight, 1,
		20000);
	scene.add(camera);
	camera.position.set(0, 150, 300);
	camera.lookAt(scene.position);
	// Add lights to scene
	var directionalLight = new THREE.DirectionalLight(0xffeedd);
	directionalLight.position.set(0, 0, 1).normalize();
	scene.add(directionalLight);
	var ambientLight = new THREE.AmbientLight(0xaaaaaa);
	scene.add(ambientLight);

	// Logger
	if (modeDebug && log == null) {
	    log = new Stats();
	    log.domElement.style.position = 'absolute';
	    log.domElement.style.bottom = '0';
	    log.domElement.style.zIndex = 9999;
	    document.body.appendChild(log.domElement);
	}
    };

    /**
     * Render screen.
     */
    this.render = function() {
	requestAnimationFrame(arguments.callee);

	// Update fps logger
	if (log != null) {
	    log.update();
	}

	// Clean the screen
	renderer.clear();
	renderer.setClearColor(0x000011);

	// Update and draw the screen
	currentScreen.update(clock.getDelta());
	currentScreen.draw(clock.getDelta());

	// Check if the current screen is done and set the next screen if return
	// null means that is not done
	nextScreen = currentScreen.nextScreen();
	if (nextScreen != null) {
	    // Dispose the resources of the current screen
	    currentScreen.dispose();
	    // Set the next screen
	    currentScreen = nextScreen;
	}

	// Render screen
	renderer.render(scene, camera);
    };

    /**
     * Pause actual screen.
     */
    this.pause = function() {
	if (currentScreen != null)
	    currentScreen.pause();
    };

    /**
     * Resume actual screen.
     */
    this.resume = function() {
	if (currentScreen != null)
	    currentScreen.resume();
    };

    /**
     * Dispose actual screen.
     */
    this.dispose = function() {
	if (currentScreen != null)
	    currentScreen.dispose();
    };

    /**
     * Resize render.
     */
    this.resize = function(width, height) {
	Resizer.WindowResize(renderer, camera, width, height);
	if (currentScreen != null)
	    currentScreen.resize(width, height);
    };

    /**
     * Check if browser have support to webgl.
     */
    this.supportsWebGL = function() {
	try {
	    return !!window.WebGLRenderingContext
		    && !!document.createElement('canvas').getContext(
			    'experimental-webgl');
	} catch (e) {
	    return false;
	}
    };

    // GETTERS & SETTERS
    /**
     * Get scene.
     * 
     * @returns the scene.
     */
    this.getScene = function() {
	return scene;
    };

    /**
     * Get camera.
     * 
     * @returns the camera.
     */
    this.getCamera = function() {
	return camera;
    };
};