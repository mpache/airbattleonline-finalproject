/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * WorldEntity.js
 * 
 * @version June 3, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var WorldEntity = function(worldType, w, h, d, t, wpns) {

    /* Fields */
    this.type = worldType;
    this.width = w;
    this.height = h;
    this.depth = d;
    this.time = t;
    this.blockEntities = [];
    this.weaponEntities = [];

    /**
     * Add block entity
     * 
     * @param blockEntity
     *                is the block entity to add.
     */
    this.addBlockEntity = function(blockEntity) {
	this.blockEntities.push(blockEntity);
    };

    /**
     * Add weapon entity.
     * 
     * @param weaponEntity
     *                is a weapon entity to add.
     */
    this.addWeaponEntity = function(weaponEntity) {
	this.weaponEntities.push(weaponEntity);
    };

    /**
     * To String.
     */
    this.toString = function() {
	return "WorldEntity [type=" + this.type + ", width=" + this.width
		+ ", height=" + this.height + ", depth=" + this.depth
		+ ", time=" + this.time + ", blocks=" + this.blockEntities
		+ ", weapons=" + this.weaponEntities + "]";
    };
};