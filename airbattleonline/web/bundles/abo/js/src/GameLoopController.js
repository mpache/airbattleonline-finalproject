/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 * 		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline2d.
 *
 * AirBattleOnline2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline2d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * GameLoopController.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var GameLoopController = function(socket, worldEntity, playerEntity, is3D) {

    /* Fields */
    var world = FactoryObject.getWorld(worldEntity, playerEntity);
    world.defineSocket(socket);
    // Initialized UI.
    var uiGameLoop = is3D ? new UI3DGameLoop(this) : new UI2DGameLoop(this);
    var dt = 0;

    /**
     * Update game loop.
     */
    this.update = function(delta) {
	dt = delta;
	if (!world.isGameOver()) {
	    world.updatePlayer(delta);
	    // world.updateEnemies;
	    // world.updateItems;
	    world.updateProjectiles(delta);
	    // world.updateBlocks;
	    world.updateTime(delta);
	}
    };

    /**
     * Render elements in game loop.
     */
    this.draw = function(delta) {
	if (world.isGameOver()) {
	    uiGameLoop.gameOver(world.getPlayer().getScore(), delta);
	} else {
	    // Render background.
	    uiGameLoop.renderBackground(world, delta);

	    // Render player.
	    uiGameLoop.renderPlayer(world.getPlayer().toString(), world
		    .getPlayer(), delta);

	    // Render enemies.
	    var enemies = world.getEnemies();
	    for (var i = 0; i < enemies.length; i++) {
		var enemy = enemies[i];
		uiGameLoop.renderEnemy(enemy.toString(), enemy, delta);
	    }

	    // Render blocks.
	    // TODO

	    // Render items.
	    // TODO

	    // Render projectiles.
	    var projectiles = world.getProjectiles();
	    for (var i = 0; i < projectiles.length; i++) {
		var projectile = projectiles[i];
		uiGameLoop.renderProjectile(projectile, delta);
	    }

	    // Update hud.
	    uiGameLoop.updateLife(world.getPlayer().getLife());
	    uiGameLoop.updateShield(world.getPlayer().getShield());
	    uiGameLoop.updateOverheating(world.getPlayer().getOverheating());
	    uiGameLoop.updateScore(world.getPlayer().getScore());
	    uiGameLoop.updateTime(world.getTime());
	}
    };

    /**
     * Resize elements in game loop.
     */
    this.resize = function(width, height) {
	uiGameLoop.resize(width, height);
    };

    /**
     * Pause this screen.
     */
    this.pause = function() {
	// nop
    };

    /**
     * Resume this screen.
     */
    this.resume = function() {
	// nop
    };

    /**
     * Dispose the ui associated to this screen.
     */
    this.dispose = function() {
	uiGameLoop.dispose();
    };

    /**
     * Get next screen.
     */
    this.nextScreen = function() {
	return uiGameLoop.nextScreen();
    };

    /**
     * Process action if the screen contains any key binding. Only used in a
     * context 2D.
     * 
     * @param {Object}
     *                Action is the key pushed.
     */
    this.processAction = function(action) {
	var actionProcesed = false;
	switch (action) {
	case KeyboardTracker.LEFT:
	    world.getPlayer().rotateRight();
	    actionProcesed = true;
	    break;
	case KeyboardTracker.RIGHT:
	    world.getPlayer().rotateLeft();
	    actionProcesed = true;
	    break;
	case KeyboardTracker.UP:
	    world.getPlayer().speedUp();
	    actionProcesed = true;
	    break;
	case KeyboardTracker.DOWN:
	    world.getPlayer().speedDown();
	    actionProcesed = true;
	    break;
	case KeyboardTracker.ARROW_UP:
	    world.getPlayer().rotateUp();
	    break;
	case KeyboardTracker.ARROW_DOWN:
	    world.getPlayer().rotateDown();
	    break;
	case KeyboardTracker.SPACE:
	    world.getPlayer().shoot(dt);
	    actionProcesed = true;
	    break;
	}
	return actionProcesed;
    };

    /**
     * Control back pressed in mobile phones.
     */
    this.onBackPressed = function() {
	return uiGameLoop.onBackPressed();
    };
};