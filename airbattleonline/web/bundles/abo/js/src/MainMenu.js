/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline2d.
 *
 * AirBattleOnline2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline2d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * MainMenuController.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var MainMenu = function() {

    /* Fields */
    // Scene and camera.
    var scene = gsc.getScene();
    var camera = gsc.getCamera();
    // Define temporal screen to create animation before change screen.
    var tmpScreen = null;
    var nextScreen = null;
    // Define elements in screen.
    var background = null;

    /**
     * Call create views and register events.
     */
    (function() {
	background = createSprite("/bundles/abo/images/nebula-xneg.png", 0, 0, innerWidth, innerHeight, 1);
	scene.add(background);
	createView();
	registerEvents();
    })();
    
    /**
     * Update this screen.
     */
    this.update = function(delta) {
	// TODO
    };

    /**
     * Draw this screen.
     */
    this.draw = function(delta) {
	// TODO
    };

    /**
     * Resize this screen.
     */
    this.resize = function(width, height) {
	if (height > 600 ) {
	    jQuery("#main-content").css("transform", "scale(1)");
	}
	else {
	    jQuery("#main-content").css({
		"transform" : "scale(0.8)",
		"margin-top" : "-6px"
	    });
	}
    };

    /**
     * Pause this screen.
     */
    this.pause = function() {
	// nop
    };

    /**
     * Resume this screen.
     */
    this.resume = function() {
	// nop
    };

    /**
     * Dispose this screen.
     */
    this.dispose = function() {
	jQuery("#main-content").remove();
	scene.remove(background);
    };

    /**
     * Put next screen.
     */
    this.nextScreen = function() {
	return nextScreen;
    };

    /**
     * If this screen contains any key binding, process it.
     * 
     * @param {Object}
     *                action is the key pressed.
     */
    this.processAction = function(action) {
	// nop
    };

    /**
     * This method is used to control key back on mobile phones.
     */
    this.onBackPressed = function() {
	// nop
    };
    
    // PRIVATE FUNCTIONS
    function createSprite(srcImage, x, y, width, height) {
	var material = new THREE.SpriteMaterial({
	    map : THREE.ImageUtils.loadTexture(srcImage),
	    useScreenCoordinates : true,
	});
	var sprite = new THREE.Sprite(material);
	sprite.position.set(x, y, -1);
	sprite.scale.set(width, height, 1.0);
	sprite.alphaTest = 0.0;
	return sprite;
    }
    
    function createView() {
	// Create links and buttons.
	var follow = '<a id="home-follow" class="main-menu-box" title="Follow us" href="javascript:void(0)">+ Follow us</a>';
	var facebook = '<a target="_blank" title="Facebook" href="https://www.facebook.com/">Facebook</a>';
	var twitter = '<a target="_blank" title="twitter" href="https://twitter.com/">Twitter</a>';
	var google = '<a target="_blank" title="google+" href="https://plus.google.com/">Google+</a>';
	var logout = '<a class="logout" title="logout" href="javascript:void(0)">';
	var openCloseOp1 = '<a id="home-options-menu1-open-close" title="Close menu" href="javascript:void(0)">';
	var openCloseOp2 = '<a id="home-options-menu1-open-close-menu2" title="Open Options" href="javascript:void(0)">';
	var play = '<a id="home-play" title="Play" href="javascript:void(0)">';
	var closeSelectGame = '<a class="home-close-select-game" title="Close" href="javascript:void(0)">X</a>';
	var game2d = '<a id="home-game-2d" title="Air Battle 2D" href="javascript:void(0)">';
	var game3d = '<a id="home-game-3d" title="Air Battle 3D" href="javascript:void(0)">';
	// Main content.
	var mainContent = '<div id="main-content" class="full-width clearfix to-upper">'
		+ '<nav class="main-nav"><ul class="wrap"><li><div class="follow"><ul class="follow-us social">'
		+ '<li>'
		+ follow
		+ '</li><li class="facebook hide">'
		+ facebook
		+ '</li><li class="twitter hide">'
		+ twitter
		+ '</li><li class="google hide">'
		+ google
		+ '</li></ul></div></li><li class="main-menu-logout"><div>'
		+ '<span class="main-menu-box main-menu-box-logout">Hello,'
		+ username
		+ '</span>'
		+ logout
		+ '<img alt="Logout" src="/bundles/abo/images/logout.png"></a></div></li></ul></nav>'
		+ '<div id="home-hud" class="wrap">'
		+ '<img id="home-title1" alt="Air Battle" src="/bundles/abo/images/home_hud_title1.png">'
		+ '<img id="home-title2" alt="Online" src="/bundles/abo/images/home_hud_title2.png">'
		+ '<div id="home-options-menu1">'
		+ '<img alt="Options Menu1" src="/bundles/abo/images/home_hud_options_menu1.png">'
		+ openCloseOp1
		+ '<img alt="Close Menu" src="/bundles/abo/images/home_hud_close_options_menu1.png">'
		+ '<img class="hide" alt="Open Menu" src="/bundles/abo/images/home_hud_open_options_menu1.png">'
		+ '</a>'
		+ openCloseOp2
		+ '<img alt="Open Options" src="/bundles/abo/images/home_hud_open_close_options_menu2.png">'
		+ '</a></div><div id="home-options-menu2" class="hide">'
		+ '<img alt="Options Menu1" src="/bundles/abo/images/home_hud_options_menu2.png"></div>'
		+ play
		+ '<img alt="Play" src="/bundles/abo/images/home_hud_core.png"></a></div>'
		+ '<div class="overlay hide"></div><div id="home-select-game" class="hide">'
		+ closeSelectGame
		+ '<h1>Select game</h1>'
		+ game2d
		+ '<img alt="Play" src="/bundles/abo/images/2d_game.png"></a>'
		+ game3d
		+ '<img alt="Play" src="/bundles/abo/images/3d_game.png"></a></div></div>';
	// Add main content to body.
	jQuery("body").append(mainContent);
    }
    
    function registerEvents() {
	// follow
	jQuery("#home-follow").click(function() {
	    if (jQuery(".follow").css("height") == "80px") {
		jQuery(".follow").animate({height : "30px"});
		jQuery(".follow-us .facebook,.twitter,.google").hide("slow");
	    } else {
		jQuery(".follow").animate({height : "80px"});
		jQuery(".follow-us .facebook,.twitter,.google").show("slow");
	    }
	});
	// logout
	jQuery(".logout").click(function() {
	    alertify.set({labels : {ok : "Exit",cancel : "Cancel"}});
	    alertify.confirm("Are you sure you want to quit the game?",function(e) {
		if (e) {
		    var url = window.location.href.split("game")[0];
		    window.location.href = url + "logout";
		} else {
		    return;
		}
	    });
	});
	// open or close options 1
	jQuery("#home-options-menu1-open-close").click(function() {
	    if (jQuery("#home-options-menu1-open-close img:first").is(":visible")) {
		jQuery("#home-options-menu1").animate({
		    margin : "270px -30px"
		}, 500, function() {
		    jQuery("#home-options-menu1-open-close").attr("title", "Open Menu");
		    jQuery("#home-options-menu1-open-close img").toggle();
		});
	    } else {
		jQuery("#home-options-menu1").show();
		jQuery("#home-options-menu1").animate({
		    margin : "270px -130px"
		}, 500, function() {
		    jQuery("#home-options-menu1-open-close").attr("title","Close Menu");
		    jQuery("#home-options-menu1-open-close img").toggle();
		});
	    }
	});
	// open or close options 2.
	jQuery("#home-options-menu1-open-close-menu2").click(function() {
	    var elem = $("#home-options-menu1-open-close-menu2");
	    if (!jQuery("#home-options-menu2").is(":visible")) {
		jQuery({deg : 0}).animate({deg : 180},
			{duration : 1000,step : function(now) {
			    elem.css({
				transform : "rotate(" + now + "deg)"
			    });
			}
		});
		jQuery("#home-options-menu2").show();
		jQuery("#home-options-menu2").animate({
		    margin : "404px -48px"
		}, 1000);
	    } else {
		jQuery({deg : 180}).animate({deg : 0},
			{duration : 1000,step : function(now) {
			    elem.css({
				transform : "rotate("+ now + "deg)"
			    });
			}
		});
		jQuery("#home-options-menu2").animate({
		    margin : "280px 70px"
		}, 1000, function() {jQuery(this).hide();});
	    }
	});
	// Select game.
	jQuery("#home-play,.home-close-select-game").click(function() {
	    jQuery("#home-select-game").center();
	    if (jQuery("#home-select-game").is(":visible")) {
		jQuery(".overlay").hide();
		jQuery("#home-select-game").toggle("slow");
	    } else {
		jQuery(".overlay").show();
		jQuery("#home-select-game").toggle("slow");
	    }
	});
	// game 2d
	jQuery("#home-game-2d").click(function() {
	    // TEST
	    nextScreen = new LoadingScreen(false);
//	    nextScreen = new SelectGameScreen();
	});
	// game 3d
	jQuery("#home-game-3d").click(function() {
	    if (gsc.supportsWebGL()) {
		// TEST
		nextScreen = new LoadingScreen(true);
	    } else { 
		alertify.alert("Your browser not " +
			"support webgl or be desactivated, at this " +
			"moment your only play game in 2d.");
	    }
	});
    };
};