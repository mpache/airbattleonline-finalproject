/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * FactoryObject.js
 * 
 * @version June 3, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var FactoryObject = function() {
};
// Static methods.

// Block
FactoryObject.getBlock = function(entity) {
    // TODO
};

// Weapon
FactoryObject.getWeapon = function(entity) {
    // Fetch data
    var w = entity.width;
    var h = entity.height;
    var d = entity.depth;
    var sp = entity.speed;
    var dmg = entity.damage;
    var dt = entity.duration;
    // create object
    return new Weapon(entity.type, w, h, d, sp, dmg, dt);;
};

// Ships
FactoryObject.getShip = function(entity) {
    // Fetch data
    var px = entity.posx;
    var py = entity.posy;
    var pz = entity.posz;
    var w = entity.width;
    var h = entity.height;
    var d = entity.depth;
    var l = entity.life;
    var s = entity.shield;
    var sp = entity.speed;

    // create object
    var ship = null;
    switch (entity.type) {
    case ShipType.SHIP2D_T1:
	ship = new Ship2DT1(px, py, w, h, l, s, sp);
	break;
    case ShipType.SHIP3D_T1:
	ship = new Ship3DT1(px, py, pz, w, h, d, l, s, sp);
	break;
    }
    // Load default weapon of ship.
    var entityWeapon = entity.defaultWeapon;
    var dw = FactoryObject.getWeapon(entityWeapon);
    ship.addWeapon(dw);
    // Return ship
    return ship;
};

// World
FactoryObject.getWorld = function(worldEntity, playerEntity) {
    // Objects
    var player = FactoryObject.getShip(playerEntity);

    // fetch data
    var w = worldEntity.width;
    var h = worldEntity.height;
    var d = worldEntity.depth;
    var t = worldEntity.time;
    // Blocks
    var bEntities = worldEntity.blockEntities;
    // weapons
    var wEntities = worldEntity.weaponEntities;

    // Create object
    var world = null;
    switch (worldEntity.type) {
    case WorldType.WORLD_G1:
	world = new WorldG1(player, w, h, d, t);
	break;
    case WorldType.WORLD_G2:
	// TODO
	break;
    case WorldType.WORLD_G3:
	// TODO
	break;
    case WorldType.WORLD_G4:
	// TODO
	break;
    }
    // Create and add blocks.
    for (var i = 0; i < bEntities.length; i++) {
	var entity = bEntities[i];
	var block = this.getBlock(entity);
	world.addBlock(block);
    }
    // Create and add weapons.
    for (var i = 0; i < wEntities.length; i++) {
	var entity = wEntities[i];
	var weapon = this.getWeapon(entity);
	world.addWeapon(weapon);
    }
    // Return world
    return world;
};