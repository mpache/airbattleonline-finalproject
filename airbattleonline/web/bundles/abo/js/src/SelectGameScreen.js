/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline2d.
 *
 * AirBattleOnline2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline2d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SelectGameScreen.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var SelectGameScreen = function() {

    // Scene and camera.
    var scene = gsc.getScene();
    var camera = gsc.getCamera();
    // Define temporal screen to create animation before change screen.
    var tmpScreen = null;
    var nextScreen = null;
    // Define elements in screen.
    var background = null;

    /**
     * Call create views and register events.
     */
    (function() {
        background = createSprite("images/nebula-xneg.png", 0, 0, innerWidth, innerHeight, 1);
        scene.add(background);
        createView();
        registerEvents();
    })();

    /**
     * Update this screen.
     */
    this.update = function(delta) {
        // TODO
    };

    /**
     * Draw this screen.
     */
    this.draw = function(delta) {
        // TODO
    };

    /**
     * Resize this screen.
     */
    this.resize = function(width, height) {
        if (height > 600) {
            jQuery("#main-content").css("transform", "scale(1)");
        }
        else {
            jQuery("#main-content").css({
                "transform": "scale(0.8)",
                "margin-top": "-6px"
            });
        }
    };

    /**
     * Pause this screen.
     */
    this.pause = function() {
        // nop
    };

    /**
     * Resume this screen.
     */
    this.resume = function() {
        // nop
    };

    /**
     * Dispose this screen.
     */
    this.dispose = function() {
        jQuery("#main-content").remove();
        scene.remove(background);
    };

    /**
     * Put next screen.
     */
    this.nextScreen = function() {
        return nextScreen;
    };

    /**
     * If this screen contains any key binding, process it.
     * 
     * @param {Object}
     *                action is the key pressed.
     */
    this.processAction = function(action) {
        // nop
    };

    /**
     * This method is used to control key back on mobile phones.
     */
    this.onBackPressed = function() {
        // nop
    };

    // PRIVATE FUNCTIONS
    function createSprite(srcImage, x, y, width, height) {
        var material = new THREE.SpriteMaterial({
            map: THREE.ImageUtils.loadTexture(srcImage),
            useScreenCoordinates: true,
        });
        var sprite = new THREE.Sprite(material);
        sprite.position.set(x, y, -1);
        sprite.scale.set(width, height, 1.0);
        sprite.alphaTest = 0.0;
        return sprite;
    }

    function createView() {
        // Main content.
        var mainContent = '<div id="main-content" class="full-width clearfix to-upper">'
            	+ '<h1 style="color: #FFF;text-align: center;margin: 50px 0px -50px;font-size: 1.5em;">'
            	+ 'Select Game</h1>'
                + '<div id="home-games" >'
                + '<ul>'
                + '<li>'
                + '<a href="javascript:void(0)"  class="main-menu-box"><div class="game">'
                + '<div>ALL VS ALL</div></div></a>'
                + '</li>'
                + '<li>'
                + '<a href="javascript:void(0)"  class="main-menu-box"><div class="game">'
                + '<div>5 VS 5</div></div></a>'
                + '</li>'
                + '<li>'
                + '<a href="javascript:void(0)"  class="main-menu-box"><div class="game">'
                + '<div>Survivor</div></div></a>'
                + '</li>'
                + '<li>'
                + '<a href="javascript:void(0)"  class="main-menu-box"><div  class="game">'
                + '<div>Training</div></div></a>'
                + '</li>'
                + '</ul></div>';
        // Add main content to body.
        jQuery("body").append(mainContent);
    }
    ;

    function registerEvents() {
        // follow
        jQuery(".follow").click(function() {
            if (jQuery(".follow").css("height") == "80px") {
                jQuery(".follow").animate({height: "30px"});
                jQuery(".follow-us .facebook,.twitter,.google").hide("slow");
            } else {
                jQuery(".follow").animate({height: "80px"});
                jQuery(".follow-us .facebook,.twitter,.google").show("slow");
            }
        });



        jQuery(".game").click(function() {
            nextScreen = new LoadingScreen();
        });

        // logout
        jQuery(".logout").click(function() {
            alertify.set({labels: {ok: "Exit", cancel: "Cancel"}});
            alertify.confirm("Are you sure you want to quit the game?", function(e) {
                if (e) {
                    // OK
                } else {
                    // Cancel
                }
            });
        });
    }
    ;
};