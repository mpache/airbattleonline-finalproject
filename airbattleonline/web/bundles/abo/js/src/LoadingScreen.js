/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline2d.
 *
 * AirBattleOnline2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline2d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * LoadingScreen.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var LoadingScreen = function(is3D) {

    //Socket
    //var socket = io('http://localhost:8080/');
    var socket = io('http://192.168.1.120:8080/');
    //var socket = io('http://172.16.205.7:8080/');
    // Scene and camera.
    var scene = gsc.getScene();
    var camera = gsc.getCamera();
    // Define temporal screen to create animation before change screen.
    var tmpScreen = null;
    var nextScreen = null;
    // Define elements in screen.
    var background = null;

    /**
     * Call create views and register events.
     */
    (function() {
        background = createSprite("/bundles/abo/images/nebula-xneg.png", 0, 0, innerWidth, innerHeight, 1);
        scene.add(background);
        createView();
    })();

    /**
     * Update this screen.
     */
    this.update = function(delta) {
        // TODO
    };

    /**
     * Draw this screen.
     */
    this.draw = function(delta) {
        // TODO
    };

    /**
     * Resize this screen.
     */
    this.resize = function(width, height) {
        if (height > 600) {
            jQuery("#main-content").css("transform", "scale(1)");
        }
        else {
            jQuery("#main-content").css({
                "transform": "scale(0.8)",
                "margin-top": "-6px"
            });
        }
    };

    /**
     * Pause this screen.
     */
    this.pause = function() {
        // nop
    };

    /**
     * Resume this screen.
     */
    this.resume = function() {
        // nop
    };

    /**
     * Dispose this screen.
     */
    this.dispose = function() {
        jQuery("#main-content").remove();
        scene.remove(background);
    };

    /**
     * Put next screen.
     */
    this.nextScreen = function() {
        return nextScreen;
    };

    /**
     * If this screen contains any key binding, process it.
     * 
     * @param {Object}
     *                action is the key pressed.
     */
    this.processAction = function(action) {
        // nop
    };

    /**
     * This method is used to control key back on mobile phones.
     */
    this.onBackPressed = function() {
        // nop
    };

    // PRIVATE FUNCTIONS
    function createSprite(srcImage, x, y, width, height) {
        var material = new THREE.SpriteMaterial({
            map: THREE.ImageUtils.loadTexture(srcImage),
            useScreenCoordinates: true,
        });
        var sprite = new THREE.Sprite(material);
        sprite.position.set(x, y, -1);
        sprite.scale.set(width, height, 1.0);
        sprite.alphaTest = 0.0;
        return sprite;
    }

    function createView() {
        var bar = '<div id="progress" class="graph"><div id="bar" style="width:0%"><p>0 players of 2</p></div></div>';
        // Main content.
        var mainContent = '<div id="main-content" class="full-width clearfix to-upper">' + bar + "</div>";
        // Add main content to body.
        jQuery("body").append(mainContent);
    }

    socket.on('connected', function(data) {
        jQuery("#bar p").text(data.players + " players of " + data.total);
        var value = data.players / data.total * 100;
        jQuery("#bar").css("width", value + "%");
        if (data.players === data.total) {
            // TEST 
            if (is3D) {
        	gsc.create3DRenderer(MODE_DEBUG);
        	SettingManager.world = WorldType.WORLD_G1;
        	SettingManager.player = ShipType.SHIP3D_T1;
     	    	var worldEntity = SettingManager.DAO.getWorldEntity(SettingManager.world);
     	    	var playerEntity = SettingManager.DAO.getShipEntity(SettingManager.player);
                nextScreen = new GameLoopController(socket, worldEntity, playerEntity, true);
            } else {
        	SettingManager.world = WorldType.WORLD_G1;
        	SettingManager.player = ShipType.SHIP2D_T1;
     	    	var worldEntity = SettingManager.DAO.getWorldEntity(SettingManager.world);
     	    	var playerEntity = SettingManager.DAO.getShipEntity(SettingManager.player);
                nextScreen = new GameLoopController(socket, worldEntity, playerEntity, false);
            }
        }
    });

    socket.on('alert', function(data) {
        alertify.alert(data);
    });
};
