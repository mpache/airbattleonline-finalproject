/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * UI3DGameLoop.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var UI3DGameLoop = function(controller) {

    /* Fields */
    // Elements of this ui
    var sceneUI = new Scene();
    var hud = new HUD();
    var characters = new Character();

    // Scene and camera
    var scene = gsc.getScene();
    var camera = gsc.getCamera();

    // input processor
    var input = new InputGameLoop(controller);

    // Tmp fields;
    var projectileMesh = null;

    // TEST
    // var axes = new THREE.AxisHelper(150);
    // scene.add(axes);

    /**
     * Update life of player.
     */
    this.updateLife = function(life) {
	hud.updateLife(life);
    };

    /**
     * Update shield of player.
     */
    this.updateShield = function(shield) {
	hud.updateShield(shield);
    };

    /**
     * Update overheating of this player.
     */
    this.updateOverheating = function(overheating) {
	hud.updateOverheating(overheating);
    };

    /**
     * Update score of player.
     */
    this.updateScore = function(score) {
	hud.updateScore(score);
    };

    /**
     * Update time remaining if exists.
     */
    this.updateTime = function(time) {
	hud.updateTime(time);
    };

    /**
     * Render player.
     */
    this.renderPlayer = function(playerType, player, delta) {
	input.checkInput();
	// render player
	var mesh = player.getMesh();
	var shieldMesh = player.getShieldMesh();
	if (mesh == null) {
	    player.setMesh("");
	    characters.create3DPlayer(playerType, player.getWidth(), player
		    .getHeight(), player.getDepth(), function(m) {
		m.add(camera);
		camera.position.set(0,7,15);
		player.setMesh(m);
		scene.add(m);
		// Shield mesh
		var shield = characters.create3DGlow(null, camera, 0.04);
		player.setShieldMesh(shield);
		scene.add(shield);
	    });
	} else if (mesh != "") {
	    // Check if player is explode
	    if (player.getState() == ShipState.EXPLODE) {
		mesh.visible = false;
	    }
	    // Check if player is respawned
	    else if (player.getState() == ShipState.RESPAWN) {
		// nop
	    }
	    // Player is alive
	    else {
		if (mesh.visible == false) {
		    mesh.visible = true;
		}
		shieldMesh.visible = player.getShield() > 0 ? true : false;
	    }
	    // Update player.
	    // -----------------------------------------------------------
	    // Shield
	    shieldMesh.position.x = player.position.x;
	    shieldMesh.position.y = player.position.y;
	    shieldMesh.position.z = player.position.z;
	    shieldMesh.rotation.x = player.getAngleX();
	    shieldMesh.rotation.z = player.getAngleZ();

	    // Mesh
	    mesh.position.x = player.position.x;
	    mesh.position.y = player.position.y;
	    mesh.position.z = player.position.z;
	    mesh.rotation.x = player.getAngleX();
	    mesh.rotation.z = player.getAngleZ();

	    // ------------------------------------------------------------
	    // Update camera.
//	    camera.position.x = player.position.x;
//	    camera.position.y = player.position.y + 8;
//	    camera.position.z = player.position.z + 20;

//	     camera.rotation.x += 0.05;
//	     camera.rotation.y += 0.05;
//	     camera.rotation.z = player.getAngleZ();
	}
    };

    /**
     * Render enemy.
     */
    this.renderEnemy = function(enemyType, enemy, delta) {
	// render player
	var mesh = enemy.getMesh();
	var shieldMesh = enemy.getShieldMesh();
	if (mesh == null) {
	    enemy.setMesh("");
	    characters.create3DPlayer(enemyType, enemy.getWidth(), enemy
		    .getHeight(), enemy.getDepth(), function(m) {
		enemy.setMesh(m);
		scene.add(m);
		// Shield mesh
		var shield = characters.create3DGlow(null, camera, 0.04);
		enemy.setShieldMesh(shield);
		scene.add(shield);
	    });
	} else if (mesh != "") {
	    // Check if Enemy is explode
	    if (enemy.getState() == ShipState.EXPLODE) {
		mesh.visible = false;
	    }
	    // Check if Enemy is respawned
	    else if (enemy.getState() == ShipState.RESPAWN) {
		// nop
	    }
	    // Enemy is alive
	    else {
		if (mesh.visible == false) {
		    mesh.visible = true;
		}
		// Shield
		shieldMesh.visible = enemy.getShield() > 0 ? true : false;
	    }
	    // -----------------------------------------------------------
	    // Update player.
	    shieldMesh.position.x = enemy.position.x;
	    shieldMesh.position.y = enemy.position.y;
	    shieldMesh.position.z = enemy.position.z;
	    shieldMesh.rotation.x = enemy.getAngleX();
	    shieldMesh.rotation.z = enemy.getAngleZ();

	    // Mesh
	    mesh.position.x = enemy.position.x;
	    mesh.position.y = enemy.position.y;
	    mesh.position.z = enemy.position.z;
	    mesh.rotation.x = enemy.getAngleX();
	    mesh.rotation.z = enemy.getAngleZ();
	}
    };

    /**
     * Render projectiles.
     */
    this.renderProjectile = function(projectile, delta) {
	// render projectile
	var mesh = projectile.getMesh();
	if (mesh == null) {
	    if (projectileMesh == null) {
		projectile.setMesh("");
		sceneUI.create3DProjectile(projectile.getType(), projectile
			.getWidth(), projectile.getHeight(), projectile
			.getDepth(), function(m) {
		    projectileMesh = m;
		    projectile.setMesh(m);
		    scene.add(m);
		});
	    } else {
		projectile.setMesh(projectileMesh);
		scene.add(projectileMesh);
	    }
	} else if (mesh != "") {
	    // Change position of projectile
	    mesh.position.x = projectile.position.x;
	    mesh.position.y = projectile.position.y;
	    mesh.position.z = projectile.position.z;
	    mesh.rotation.x = projectile.getAngleX();
	    mesh.rotation.z = projectile.getAngleZ();
	}
    };

    /**
     * Render items.
     */
    this.renderItems = function(item, delta) {
	// TODO
    };

    /**
     * Render blocks.
     */
    this.renderBlocks = function(block, delta) {
	// TODO
    };

    /**
     * Render background.
     */
    this.renderBackground = function(world, delta) {
	if (world.getStarSystem() == null) {
	    var sky = sceneUI.create3DStarSystem(world.getWidth(), world
		    .getHeight(), world.getDepth());
	    world.setStarSystem(sky);
	    scene.add(sky);
	}
    };

    /**
     * Pause game.
     */
    this.pauseGame = function() {
	hud.pauseGame();
    };

    /**
     * Resume game.
     */
    this.resumeGame = function() {
	hud.resumeGame();
    };

    /**
     * Game over.
     */
    this.gameOver = function(finalScore, delta) {
	hud.gameOver(finalScore, delta);
    };

    /**
     * Resize game.
     */
    this.resize = function(width, height) {
	hud.resize(width, height);
    };

    /**
     * Dispose game.
     */
    this.dispose = function() {
	hud.dispose();
    };

    /**
     * Get next screen.
     */
    this.nextScreen = function() {
	return hud.nextScreen();
    };

    /**
     * Control back key in moblie phones.
     */
    this.onBackPressed = function() {
	return hud.onBackPressed();
    };
};