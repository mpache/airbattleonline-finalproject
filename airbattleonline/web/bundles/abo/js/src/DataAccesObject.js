/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * DataAccesObject.js
 * 
 * @version June 3, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var DataAccesObject = function() {

    /* CONSTANTS */
    var URL = "/bundles/abo/database/database.json";
    // Main fields
    var JSON_FIELD_BLOCKS = "blocks";
    var JSON_FIELD_WEAPONS = "weapons";
    var JSON_FIELD_SHIPS = "ships";
    var JSON_FIELD_WORLDS = "worlds";
    // Blocks json fields
    var JSON_FIELD_BLOCK_TYPE = "name";
    // TODO
    // Weapon json fields
    var JSON_FIELD_WEAPON_TYPE = "name";
    var JSON_FIELD_WEAPON_WIDTH = "width";
    var JSON_FIELD_WEAPON_HEIGHT = "height";
    var JSON_FIELD_WEAPON_DEPTH = "depth";
    var JSON_FIELD_WEAPON_SPEED = "speed";
    var JSON_FIELD_WEAPON_DAMAGE = "damage";
    var JSON_FIELD_WEAPON_DURATION = "duration";
    // ships json fields
    var JSON_FIELD_PLAYER_POSX = "posx";
    var JSON_FIELD_PLAYER_POSY = "posy";
    var JSON_FIELD_PLAYER_POSZ = "posz";
    var JSON_FIELD_PLAYER_WIDTH = "width";
    var JSON_FIELD_PLAYER_HEIGHT = "height";
    var JSON_FIELD_PLAYER_DEPTH = "depth";
    var JSON_FIELD_PLAYER_LIFE = "life";
    var JSON_FIELD_PLAYER_SHIELD = "shield";
    var JSON_FIELD_PLAYER_SPEED = "speed";
    var JSON_FIELD_PLAYER_DEFAULT_WEAPON = "defaultWeapon";
    // world json fields
    var JSON_FIELD_WORLD_WIDTH = "width";
    var JSON_FIELD_WORLD_HEIGHT = "height";
    var JSON_FIELD_WORLD_DEPTH = "depth";
    var JSON_FIELD_WORLD_TIME = "time";
    // Data in Objects
    var jBlocks = [];
    var jWeapons = [];
    var jShips = [];
    var jWorlds = [];

    /**
     * Load
     */
    (function() {
	getJSONArray(URL, JSON_FIELD_BLOCKS, function(array) {
	    jBlocks = array;
	});
	getJSONArray(URL, JSON_FIELD_WEAPONS, function(array) {
	    jWeapons = array;
	});
	getJSONArray(URL, JSON_FIELD_SHIPS, function(array) {
	    jShips = array;
	});
	getJSONArray(URL, JSON_FIELD_WORLDS, function(array) {
	    jWorlds = array;
	});
    })();

    // PRIVATE METHOD
    function getJSONArray(url, jsonField, fn) {
	jQuery.getJSON(url, function(data) {
	    var items = [];
	    $.each(data[jsonField], function(key, val) {
		items.push(val);
	    });
	    fn(items);
	});
    }

    function getPositionByType(array, type) {
	for (var i = 0; i < array.length; i++) {
	    var obj = array[i];
	    for ( var key in obj) {
		var attrName = key;
		var attrValue = obj[key];
		if (attrName == "name" && attrValue == type) {
		    return i;
		}
	    }
	}
	return -1;
    }

    /**
     * Get block entity
     * 
     * @param blockType
     *                is a block type to read entity.
     * 
     * @returns the entity that forms a block.
     */
    this.getBlockEntity = function(blockType) {
	// TODO
    };

    /**
     * Get weapon entity
     * 
     * @param weaponType
     *                is a weapon type to read entity.
     * 
     * @returns the entity that forms a weapon.
     */
    this.getWeaponEntity = function(weaponType) {
	// Fields to create entity
	var w = 0;
	var h = 0;
	var d = 0;
	var sp = 0;
	var dmg = 0;
	var dt = 0;
	// Load fields
	var position = getPositionByType(jWeapons, weaponType);
	if (position != -1) {
	    var obj = jWeapons[position];
	    for ( var key in obj) {
		var attrName = key;
		var attrValue = obj[key];
		if (attrName == JSON_FIELD_WEAPON_WIDTH)
		    w = attrValue;
		if (attrName == JSON_FIELD_WEAPON_HEIGHT)
		    h = attrValue;
		if (attrName == JSON_FIELD_WEAPON_DEPTH)
		    d = attrValue;
		if (attrName == JSON_FIELD_WEAPON_SPEED)
		    sp = attrValue;
		if (attrName == JSON_FIELD_WEAPON_DAMAGE)
		    dmg = attrValue;
		if (attrName == JSON_FIELD_WEAPON_DURATION)
		    dt = attrValue;
	    }
	}
	return new WeaponEntity(weaponType, w, h, d, sp, dmg, dt);
    };

    /**
     * Get ship entity.
     * 
     * @param shipType
     *                is a ship type to read entity.
     * 
     * @returns the entity that forms a ship.
     */
    this.getShipEntity = function(shipType) {
	// Fields to create entity
	var type = shipType;
	var px = 0;
	var py = 0;
	var pz = 0;
	var w = 0;
	var h = 0;
	var d = 0;
	var l = 0;
	var s = 0;
	var sp = 0;
	var dw = 0;
	// Load fields
	var position = getPositionByType(jShips, shipType);
	if (position != -1) {
	    var obj = jShips[position];
	    for ( var key in obj) {
		var attrName = key;
		var attrValue = obj[key];
		if (attrName == JSON_FIELD_PLAYER_POSX)
		    px = attrValue;
		if (attrName == JSON_FIELD_PLAYER_POSY)
		    py = attrValue;
		if (attrName == JSON_FIELD_PLAYER_POSZ)
		    pz = attrValue;
		if (attrName == JSON_FIELD_PLAYER_WIDTH)
		    w = attrValue;
		if (attrName == JSON_FIELD_PLAYER_HEIGHT)
		    h = attrValue;
		if (attrName == JSON_FIELD_PLAYER_DEPTH)
		    d = attrValue;
		if (attrName == JSON_FIELD_PLAYER_LIFE)
		    l = attrValue;
		if (attrName == JSON_FIELD_PLAYER_SHIELD)
		    s = attrValue;
		if (attrName == JSON_FIELD_PLAYER_SPEED)
		    sp = attrValue;
		if (attrName == JSON_FIELD_PLAYER_DEFAULT_WEAPON) {
		    var wtype = "";
		    var ww = 0;
		    var wh = 0;
		    var wd = 0;
		    var wsp = 0;
		    var wdmg = 0;
		    var wdt = 0;
		    for ( var k in attrValue) {
			var wattrName = k;
			var wattrValue = attrValue[k];
			if (wattrName == JSON_FIELD_WEAPON_TYPE)
			    wtype = wattrValue;
			if (wattrName == JSON_FIELD_WEAPON_WIDTH)
			    ww = wattrValue;
			if (wattrName == JSON_FIELD_WEAPON_HEIGHT)
			    wh = wattrValue;
			if (wattrName == JSON_FIELD_WEAPON_DEPTH)
			    wd = wattrValue;
			if (wattrName == JSON_FIELD_WEAPON_SPEED)
			    wsp = wattrValue;
			if (wattrName == JSON_FIELD_WEAPON_DAMAGE)
			    wdmg = wattrValue;
			if (wattrName == JSON_FIELD_WEAPON_DURATION)
			    wdt = wattrValue;
		    }
		    dw = new WeaponEntity(wtype, ww, wh, wd, wsp, wdmg, wdt);
		}
	    }
	}
	// Create and return entity
	return new ShipEntity(type, px, py, pz, w, h, d, l, s, sp, dw);
    };

    /**
     * Get ship entity.
     * 
     * @param worldType
     *                is a world type to read entity.
     * 
     * @returns the entity that forms a world.
     */
    this.getWorldEntity = function(worldType) {
	// Fields to create entity
	var type = worldType;
	var w = 0;
	var h = 0;
	var d = 0;
	var t = 0;
	// Load fields
	var position = getPositionByType(jWorlds, worldType);
	if (position != -1) {
	    var obj = jWorlds[position];
	    for ( var key in obj) {
		var attrName = key;
		var attrValue = obj[key];
		if (attrName == JSON_FIELD_WORLD_WIDTH)
		    w = attrValue;
		if (attrName == JSON_FIELD_WORLD_HEIGHT)
		    h = attrValue;
		if (attrName == JSON_FIELD_WORLD_DEPTH)
		    d = attrValue;
		if (attrName == JSON_FIELD_WORLD_TIME)
		    t = attrValue;
	    }
	}
	// Create world entity
	var worldEntity = new WorldEntity(type, w, h, d, t);
	// Create block entities.
	for (var i = 0; i < jBlocks.length; i++) {
	    var obj = jBlocks[i];
	    for ( var key in obj) {
		var attrName = key;
		var attrValue = obj[key];
		if (attrName == JSON_FIELD_BLOCK_TYPE) {
		    var blockEntity = this.getBlockEntity(attrValue);
		    worldEntity.addBlockEntity(blockEntity);
		}
	    }
	}
	// Create weapons entities
	for (var i = 0; i < jWeapons.length; i++) {
	    var obj = jWeapons[i];
	    for ( var key in obj) {
		var attrName = key;
		var attrValue = obj[key];
		if (attrName == JSON_FIELD_WEAPON_TYPE) {
		    var weaponEntity = this.getWeaponEntity(attrValue);
		    worldEntity.addWeaponEntity(weaponEntity);
		}
	    }
	}
	// Return world entity.
	return worldEntity;
    };
};