/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Projectile.js
 * 
 * @version June 4, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var Projectile = function(p, x, y, z, aX, aY, aZ, type, w, h, d, sp, dmg, dt,
	is3D) {
    DynamicBody.call(this, x, y, z, w, h, d, is3D);

    /* Constants */
    var SPEED = sp;
    var DURATION = dt;
    var DAMAGE = dmg;

    /* Fields */
    var id = null;
    var projectileType = type;
    var player = p;
    var angleX = aX;
    var angleY = aY;
    var angleZ = aZ;
    var time = 0;
    // Mesh
    var mesh = null;

    /**
     * Speed up.
     */
    this.speedUp = function() {
	this.velocity.multiplyScalar(0.96);
	if (is3D) {
	    this.velocity.x -= Math.sin(angleZ) * Math.cos(angleX) * SPEED;
	    this.velocity.y += Math.cos(angleZ) * Math.sin(angleX) * SPEED;
	    this.velocity.z -= Math.cos(angleX) * SPEED;
	} else {
	    this.velocity.x += Math.cos(angleX) * SPEED;
	    this.velocity.y += Math.sin(angleX) * SPEED;
	}
    };

    /**
     * Check if projectile is over.
     * 
     * @param delta
     *                is the time elapsed.
     */
    this.isOver = function(delta) {
	time += delta;
	if (time >= DURATION) {
	    gsc.getScene().remove(mesh);
	    player.getWorld().removeProjectile(this);
	}
    };

    // GETTERS & SETTERS
    /**
     * Get id of projectile.
     * 
     * @returns the id of projectile.
     */
    this.getId = function() {
	return id;
    };

    /**
     * Set id of projectile
     * 
     * @param newId
     *                is a String.
     */
    this.setId = function(newId) {
	id = newId;
    };

    /**
     * Get player.
     * 
     * @returns a player.
     */
    this.getPlayer = function() {
	return player;
    };

    /**
     * Get mesh of projectile.
     * 
     * @returns the mesh of this projectile.
     */
    this.getMesh = function() {
	return mesh;
    };

    /**
     * Set mesh of this projectile.
     * 
     * @param newMesh
     *                is the new mesh of this projectile.
     */
    this.setMesh = function(newMesh) {
	mesh = newMesh;
    };

    /**
     * Get type of this projectile.
     * 
     * @returns the type of this projectile.
     */
    this.getType = function() {
	return projectileType;
    };

    /**
     * Get speed.
     * 
     * @returns float.
     */
    this.getSpeed = function() {
	return SPEED;
    };

    /**
     * Get damage.
     * 
     * @returns float.
     */
    this.getDamage = function() {
	return DAMAGE;
    };

    /**
     * Get duration.
     * 
     * @returns float.
     */
    this.getDuration = function() {
	return DURATION;
    };

    /**
     * Get angle of this projectile.
     * 
     * @returns the angle in x axis of this projectile.
     */
    this.getAngleX = function() {
	return angleX;
    };

    /**
     * Set angle in x axis.
     * 
     * @param newAngleX
     *                is the new angle.
     */
    this.setAngleX = function(newAngleX) {
	angleX = newAngleX;
    };

    /**
     * Get angle of this projectile.
     * 
     * @returns the angle in y axis of this projectile.
     */
    this.getAngleY = function() {
	return angleY;
    };

    /**
     * Set angle in y axis.
     * 
     * @param newAngleY
     *                is the new angle.
     */
    this.setAngleY = function(newAngleY) {
	angleY = newAngleY;
    };

    /**
     * Get angle of this projectile.
     * 
     * @returns the angle in z axis of this projectile.
     */
    this.getAngleZ = function() {
	return angleZ;
    };

    /**
     * Set angle in z axis.
     * 
     * @param newAngleZ
     *                is the new angle.
     */
    this.setAngleZ = function(newAngleZ) {
	angleZ = newAngleZ;
    };

    /**
     * Determine if this projectile is in 3d.
     * 
     * @returns true if projectile is in 3d, false otherwise.
     */
    this.is3D = function() {
	return is3D;
    };
};

// Extends of DynamicBody.
Projectile.prototype = Object.create(DynamicBody.prototype);
Projectile.prototype.constructor = Projectile;