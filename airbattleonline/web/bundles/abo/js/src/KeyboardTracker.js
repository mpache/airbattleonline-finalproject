/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *		  Jose luis Mas Rodriguez <jlmaro93@gmail.com>
 *
 * This file is part of AirBattleOnline2d.
 *
 * AirBattleOnline2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline2d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * KeyboardTracker.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
KeyboardTracker = new (function() {
    this.keysPressed = {};
    this.UP = 87;
    this.LEFT = 65;
    this.RIGHT = 68;
    this.DOWN = 83;
    this.SPACE = 32;
    this.ARROW_UP = 38;
    this.ARROW_DOWN = 40;
    this.isKeyDown = function(key) {
	if (typeof key == 'string')
	    key = key.charCodeAt(0);
	return (this.keysPressed[key]);
    };
    document.addEventListener("keydown", function(e) {
	KeyboardTracker.keysPressed[e.keyCode] = true;
    });
    document.addEventListener("keyup", function(e) {
	KeyboardTracker.keysPressed[e.keyCode] = false;
    });
})();