/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * WorldG1.js
 * 
 * @version May 19, 2014
 * @author Adrián López González
 * @author Jose luis Mas Rodriguez
 */
var WorldG1 = function(player, w, h, d, t) {
    World.call(this, player, w, h, d, t);

    /**
     * Determine if game is over.
     * 
     * @returns true if game is over, false otherwise.
     */
    this.isGameOver = function() {
	if (this.getTime() == 0) {
	    this.getSocket().disconnect();
	    this.getSocket().removeAllListeners();
	    return true;
	}
	return false;
    };
};

// Extends of World.
WorldG1.prototype = Object.create(World.prototype);
WorldG1.prototype.constructor = WorldG1;