/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of airbattleonline.
 *
 * airbattleonline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * airbattleonline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with airbattleonline.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Initialized.
 */
jQuery(function() {
    jQuery("#loginbtn").click(function() {
	if (jQuery("#login").is(":visible")) {
	    jQuery("#login").hide();
	    jQuery("#loginbtn").html("Login");
	} else {
	    if (jQuery("#loginbtn").html() == "Login") {
		jQuery("#login").show();
		jQuery("#loginbtn").html("Close");
	    }
	}
    });

    jQuery('#login-dialog').center();
});

jQuery(window).resize(function() {
    jQuery('#login-dialog').center();
})